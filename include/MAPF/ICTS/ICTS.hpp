/**
* @author Diego Planchenstainer and Giovanni Lorenzini
* @file ICTS.hpp
* @brief Header file containing the class for ICTS solver.
*/

#ifndef MAOF_ICTS_HPP
#define MAOF_ICTS_HPP

// Library includes
#include <MAPF/iMAPF.hpp>
#include <GraphNode.hpp>
#include <MDD.hpp>
#include <Hash.hpp>
#include <MAPF/ICTS/NodeAgentLabel.hpp>

// System includes
#include <iomanip>
#include <queue>
#include <stack>
#include <set>
#include <map>
#include <unordered_map>

typedef std::vector<size_t> ICTNode;

struct AgentCostPair
{
  size_t agentID;
  size_t cost;

  AgentCostPair() {}

  AgentCostPair(size_t agentID, size_t cost) : agentID(agentID), cost(cost) {}

  bool operator==(const AgentCostPair & other) const
  {
    return agentID == other.agentID && cost == other.cost;
  }
};

namespace std
{
template <>
struct hash<AgentCostPair>
{
  std::size_t operator()(const AgentCostPair & ac) const noexcept
  {
    size_t seed = 0;

    hash_combine(seed, ac.agentID);
    hash_combine(seed, ac.cost);

    return seed;
  }
};
}  // namespace std

/**
* @brief Increasing Cost Tree Search Solver, inherits from MAPFSolver.
*
* Uses an high level search (BFS) on the ICTNodes increasing a single agent's cost each time a solution is not found.
* For every ICTNode performs a goalTest that builds ICTNode.size() MDDs and search (DFS) between their combination to find a solution.
*/
class ICTSSolver : public MAPFSolver
{
private:
  // Activate/Disable Independence Detection
  bool ID = false;

  /**
  * @brief Build the MDD of depth costLimit related to an Agent agent.
  * @param agent Agent of which path from initPos to endPos needs to be found.
  * @param costLimit Depth of the solution.
  * @param heuristicCache Structure where all the heuristics are stored for avoiding re-generation.
  * @returns The generated MDD of length costLimit.
  */
  MDD buildMDD(
    Agent * agent, size_t costLimit,
    std::unordered_map<NodeAgentLabel, size_t> & heuristicCache);

  /**
  * @brief Build the MDDs for each agent at the cost specified in costs, leverage on mddsCache to avoid re-generating an MDD.
  * @param agents Agents of which path from initPos to endPos needs to be found.
  * @param costs Depth of the solution of each agent.
  * @param mddsCache Structure where all the MDDs are stored for avoiding re-generation.
  * @param heuristicCache Structure where all the heuristics are stored for avoiding re-generation.
  * @returns The MDDs associated to the given agents.
  */
  std::vector<MDD> buildMDDs(
    std::vector<Agent *> & agents, std::vector<size_t> const & costs,
    std::unordered_map<AgentCostPair, MDD> & mddsCache,
    std::unordered_map<NodeAgentLabel, size_t> & heuristicCache);

  /**
  * @brief Main function that runs Iterative Cost Tree Search.
  *   The name reacall to the Independence Detection process where the different conflicting agents are solved in different groups.
  * @param agents Agents of which path from initPos to endPos needs to be found.
  * @returns The planned path for every agent without conflicts.
  */
  std::vector<std::vector<GraphNode>> conflictingAgentsSolve(std::vector<Agent *> & agents);

  /**
  * @brief Check whether a conflict (either vertex or swap/edge) is present between the two paths.
  * @param path1,path2 The paths that need to be checked.
  * @returns true if conflict is present.
  */
  bool checkPathsConflicts(std::vector<GraphNode> & path1, std::vector<GraphNode> & path2);

  /**
  * @brief Check whether the node of the implicit product between MDDs is legal.
  The nodes will be in form {node of agent 1, node of agent 2} for a product between 2 agents.
  * @param child Node where vertex conflict and swap conflict are checked.
  * @param parent Node needed to check swap conflict between child and parents.
  * @returns true if the node is legal.
  */
  bool isLegal(std::vector<MDDNode> & child, std::vector<MDDNode> & parent);

  /**
  * @brief Enhanced Pairwise Pruning (EPP or 2E) to prune the ICT node.
  * Check pairwise if the mdds have a solution until either no solution is returned once or all the mdds do not have conflicts.
  * In the first case the node needs to be pruned, in the second case the goalTest is to be performed on all the agents.
  * @param mdds MDDs that need to be checked, should be more than 2.
  * @returns true if the node can be pruned (goalTest can be skipped)
  */
  bool prune(std::vector<MDD*> & mdds);

  /**
  * @brief search (DFS) through the implicit product between MDDs to find a solution to the MAPF problem.
  * @param mdds MDDs that need to be combined to find the path.
  * @param pruning Set to true if pairwise enhanced pruning (2E) has to be performed.
  * @returns solution of MAPF problem, {} if it not exists.
  */
  std::vector<std::vector<GraphNode>> goalTest(
    std::vector<MDD*> & mdds, bool pruning = false);

public:
  ICTSSolver() : MAPFSolver() {}

  ICTSSolver(std::vector<Agent> agents) : MAPFSolver(agents) {}

  std::vector<std::vector<GraphNode>> solve() override;
  std::vector<std::vector<GraphNode>> solveICR(const std::vector<size_t>& times) override;
  /**
  * @brief Activate the Independence Detection.
  */
  void enableID() { ID = true; }

  void postProcess(std::vector<std::vector<GraphNode>> & n) override
  {
    std::cout << "postProcess " << n.size() << std::endl;
  }

  [[nodiscard]] std::string out() const override
  {
    std::stringstream stream;
    stream << "Using ICTS" << std::endl;
    stream << "ID: " << (this->ID ? "enabled" : "disabled") << std::endl;
    stream << "Number of agents: " << this->getAgents().size() << std::endl;
    stream << "Number of nodes: " << this->getAgents()[0].getSolver()->getGraph()->getNNodes() << std::endl;
    stream << tprintf("Nodes: @\n", this->getAgents()[0].getSolver()->getGraph()->getNodes());

    stream << "Agents:" << std::endl;
    for (const Agent & a : this->getAgents()) {
      stream << tprintf("- @ @->@ goals: @\n", a, a.getInitPos(), a.getEndPos(), a.getGoals());
    }

//    stream << "Graph\n" << this->getAgents()[0].getSolver()->getGraph()->toString() << std::endl;

    return stream.str();
  }

  friend std::ostream & operator<<(std::ostream & stream, const ICTSSolver & sol)
  {
    stream << sol.out() << std::endl;
    return stream;
  }
};

#endif  //MAOF_ICTS_HPP
