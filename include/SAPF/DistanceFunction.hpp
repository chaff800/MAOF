/**
* @author Enrico Saccon (enrico.saccon@unitn.it)
* @file DistanceFunction.hpp
* @brief Contains functions used to compute the distance between two nodes.
*/

#ifndef MAOF_DISTANCEFUNCTION_HPP
#define MAOF_DISTANCEFUNCTION_HPP

// Library includes
#include <GraphNode.hpp>

// System includes
#include <cmath>

typedef double (*heurFunc)(GraphNode, GraphNode);

inline double defaultHeur(GraphNode n1, GraphNode n2) { return 0.0; }

inline double manhattan(GraphNode s, GraphNode f)
{
  return std::abs(s.getX() - f.getX()) + std::abs(s.getY() - f.getY());
}

inline double euclidean(GraphNode s, GraphNode f)
{
  return std::sqrt(std::pow(s.getX() - f.getX(), 2) + std::pow(s.getY() - f.getY(), 2));
}

inline heurFunc getHeuristic(const std::string & s)
{
  if (s == "manhattan")
    return manhattan;
  else if (s == "euclidean")
    return euclidean;
  else
    return defaultHeur;
}

#endif  //MAOF_DISTANCEFUNCTION_HPP
