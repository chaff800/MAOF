/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file A_TDSP.cpp
 * @brief 
 */

#include <SAPF/A_TDSP.hpp>
#include <SAPF/Tree.hpp>

enum class Move { YES, NO };

struct Action {
  GraphNode node;
  Move move;
  size_t time;
};

std::vector<GraphNode> A_TDSPSolver::solve(
  GraphNode initPos,
  GraphNode endPos,
  std::vector<GraphNode> goals,
  size_t depTime,
  int a_id)
{
//  GraphNode node = initPos;
//
//  // Vector to keep track of which actions have not been checked yet.
//  std::vector<Action> toBeChecked = {};
//
//  // Store visited nodes
//  TreeNode visited (nullptr, {});
//
//  size_t globalTime = depTime;
//
//  do {
//    visited[node.getId()] = node;
//
//    // Populate with possible movements
//    toBeChecked.push_back(Action{node, Move::YES, globalTime+1});
//    for (GraphNode& n : connect.get()[0]->getNeighbors(node, globalTime)){
//      if (n != node) {
//        toBeChecked.push_back(Action{n, Move::YES, globalTime+1});
//      }
//    }
//
//    // Select next movement, we want to go towards a feasible solution
//
//    // If all the nodes have been visited, then exit.
//    if (visited == connect.get()[0]->getNodes()){
//      break;
//    }
//  } while (!toBeChecked.empty());
}