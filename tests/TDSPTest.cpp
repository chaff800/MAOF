/**
* @author Enrico Saccon enrico.saccon@unitn.it
* @file TDSPTest.cpp
* @brief Tests for SAPF Time Dependent Shortest Path (TDSP).
*/

#ifdef GTEST

// System includes
#include <gtest/gtest.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>
namespace fs = std::filesystem;

// Library includes
#include "SAPF/TDSP/TDSP.hpp"
#include <Test.hpp>
#include <nlohmann/json.hpp>

typedef GraphNode Node;

TEST(TDSP, TDSP)
{
  // 1-2-3
  //   | |
  //   4 5
  //   | |
  // 6-7-8

  std::vector<Node> nodes = {Node(1), Node(2), Node(3), Node(4),
                             Node(5), Node(6), Node(7), Node(8)};

  std::vector<std::vector<int>> connect = {{0, 1, 0, 0, 0, 0, 0, 0}, {1, 0, 1, 1, 0, 0, 0, 0},
                                           {0, 1, 0, 0, 1, 0, 0, 0}, {0, 1, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 1, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 0, 1, 0, 1, 0, 1}, {0, 0, 0, 0, 1, 0, 1, 0}};

  std::vector<Node> goals = {};
  std::shared_ptr<Graph> graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));

  TDSPSolver solver(graph);
  std::vector<Node> path = solver.solve(Node(1), Node(6), goals);

  std::vector<Node> sol = {Node(1), Node(2), Node(4), Node(7), Node(6)};

  EXPECT_EQ(path.size(), sol.size()) << "path and sol do not have the same lengths "
                                     << path.size() << " " << sol.size();

  EXPECT_TRUE(equalPaths(path, sol)) << "Path is not the correct solution\n"
                                     << pathToString(path) << std::endl
                                     << pathToString(sol);
}

TEST(TDSP, TDSPWithGoals)
{
  // 1-2-3
  //   | |
  //   4 5
  //   | |
  // 6-7-8

  std::vector<Node> nodes = {Node(1), Node(2), Node(3), Node(4),
                             Node(5), Node(6), Node(7), Node(8)};

  std::vector<std::vector<int>> connect = {{0, 1, 0, 0, 0, 0, 0, 0}, {1, 0, 1, 1, 0, 0, 0, 0},
                                           {0, 1, 0, 0, 1, 0, 0, 0}, {0, 1, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 1, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 0, 1, 0, 1, 0, 1}, {0, 0, 0, 0, 1, 0, 1, 0}};

  std::vector<Node> goals = {Node(3), Node(4)};

  std::shared_ptr<Graph> graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));

  TDSPSolver solver(graph);

  std::vector<Node> path = solver.solve(Node(1), Node(6), goals);

  std::vector<Node> sol = {Node(1), Node(2), Node(3), Node(2), Node(4), Node(7), Node(6)};

  EXPECT_EQ(path.size(), sol.size())
    << "path and sol do not have the same length " << path.size() << " " << sol.size();

  EXPECT_TRUE(equalPaths(path, sol)) << "Path is not the correct solution\n"
                                     << pathToString(path) << std::endl
                                     << pathToString(sol);
}

TEST(TDSP, ShortestPathTest)
{
  for (const auto & filename : fs::directory_iterator("SPTests"))
  {
    if (std::string(filename.path()).find("json") != std::string::npos)
    {
      std::cout << "Running test " << filename.path() << std::endl;

      Node source, target;
      std::vector<Node> goals = {};
      std::vector<Node> path = {};
      std::vector<Node> sol = {};

      std::shared_ptr<Graph> graph = readSAPFJSON(sol, source, target, goals, filename.path());

      TDSPSolver solver(graph);
      try {
        path = solver.solve(source, target, goals);
      } catch (std::exception& E) {
        std::cerr << tprintf("Exception raised @", E.what()) << std::endl;
        path = {};
      }

      EXPECT_EQ(path.size(), sol.size())
        << "path and sol do not have the same length " << path.size() << " " << sol.size() << std::endl
        << solver.getGraph()->toString()
        << printTablePaths({path, sol});
      if (path.size() == sol.size()){
        break;
      }

      EXPECT_TRUE((equalPaths(path, sol)) || bothValid(path, sol, graph.get()->getMatrix()))
        << "Path is not the correct solution\n"
        << pathToString(path) << std::endl
        << pathToString(sol);
      if (!((equalPaths(path, sol)) || bothValid(path, sol, graph.get()->getMatrix()))){
        break;
      }
    }
    else {
      std::cout << filename.path() << std::endl;
    }
  }
}

#endif  // GTEST
