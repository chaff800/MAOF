/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @file Constraint.cpp
 * @brief Functions for Constraints and conflicts
 */
#include <MAPF/Constraint.hpp>

Conflict::TYPE Constraint::checkConflict(GraphNode n1, size_t t, GraphNode n2)
{
  if (
    n2 == this->getN2() && n1 == this->getN1() &&
    this->getTimestamp() == t) {  //Check edge conflict
    return Conflict::SWAP;
  }

  if (this->getN1() == n1 && this->getTimestamp() == t) {  //Check vertex conflict
    return Conflict::VERTEX;
  }

  return Conflict::NONE;
}

Conflict::TYPE Constraint::checkConflict(std::vector<GraphNode> path)
{
  if (
    this->getTimestamp() < (path.size() - 1) && path[this->getTimestamp()] == this->getN1() &&
    path[this->getTimestamp() + 1] == this->getN2()) {
    return Conflict::SWAP;
  }

  if (path[this->getTimestamp()] == this->getN1()) {  // Check vertex conflict
    return Conflict::VERTEX;
  }

  return Conflict::NONE;
}

std::vector<Constraint> getConstraintsFromConflicts(Conflict conflict)
{
  std::vector<Constraint> constraints = {};
  switch(conflict.getType()){
    case Conflict::VERTEX: {
      constraints.push_back(
        Constraint(conflict.getNode(0), conflict.getTime(), conflict.getAgent(0)));
      constraints.push_back(
        Constraint(conflict.getNode(0), conflict.getTime(), conflict.getAgent(1)));
      break;
    }
    case Conflict::SWAP: {
      constraints.push_back(
        Constraint(conflict.getNode(1), conflict.getTime(), conflict.getAgent(0)));
      constraints.push_back(
        Constraint(conflict.getNode(0), conflict.getTime(), conflict.getAgent(1)));
      break;
    }
    case Conflict::NONE: default: break;
  }
  return constraints;
}