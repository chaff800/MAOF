/**
 * @author Enrico Saccon (enrico.saccon@studenti.unitn.it)
 * @file Agent.hpp
 * @brief Contains the description of an agent.
 */

#ifndef MAOF_AGENT_HPP
#define MAOF_AGENT_HPP

// Library includes
#include <Utils.hpp>
#include <SAPF/SAPFSolver.hpp>

// System includes
#include <iostream>
#include <sstream>
#include <utility>

/**
 * @brief Class to model a robotic agent.
 * @details Each agent has its own SAPF solver. Each solver has a shared pointer to the graph.
 * Each agent SHOULD store its path to be followed.
 */
class Agent
{
private:
  int priority, ID;
  GraphNode initPos;
  GraphNode endPos;
  std::vector<GraphNode> goals;
  std::string name;
  std::unique_ptr<SAPFSolver* > solver;
  std::vector<GraphNode> path;

public:

  /**
   * @brief Void constructor
   */
  Agent()
  : priority(0), ID(0), name(), solver(std::unique_ptr<SAPFSolver*>(nullptr))
  {
  }
  /**
   * Constructor that takes an id and a SAPF solver
   * @param _id The id of the agent, i.e., in a vector the position it would occupy
   * @param _solver A pointer to the SAPF solver to be used
   */
  Agent(int _id, std::unique_ptr<SAPFSolver *> _solver=nullptr)
  : priority(0), ID(_id), name(), solver(std::move(_solver))
  {
  }
  /**
   * Constructor that takes and id, a priority and a SAPF solver
   * @param _id The id of the agent, i.e., in a vector the position it would occupy
   * @param _priority The priority assigned to the agent, useful when using priority planning (PP)
   * @param _solver A pointer to the SAPF solver to be used
   * @param _name The name of the agent, default is ""
   */
  Agent(int _id, int _priority, std::unique_ptr<SAPFSolver *> _solver=nullptr, std::string _name = "")
  : priority(_priority), ID(_id), name(_name), solver(std::move(_solver))
  {
  }
  /**
   * Constructor that takes all the possible options for the agent
   * @param _id The id of the agent, i.e., in a vector the position it would occupy
   * @param _priority The priority assigned to the agent, useful when using priority planning (PP)
   * @param _initPos The initial position the agent starts from
   * @param _endPos The final position the agent should reach
   * @param _goals A vector of goals that the agent should go through
   * @param _solver A unique pointer to the SAPF solver to be used
   * @param _name The name of the agent
   */
  Agent(
    int _id, int _priority, GraphNode _initPos, GraphNode _endPos, std::vector<GraphNode> & _goals,
    std::unique_ptr<SAPFSolver*> _solver, std::string _name = "")
  : priority(_priority),
    ID(_id),
    initPos(_initPos),
    endPos(_endPos),
    goals(_goals),
    name(std::move(_name)),
    solver(std::move(_solver))
  {
  }
  /**
   * Copy constructor
   * @param A The agent that is going to be copied into the new object
   */
  Agent(const Agent & A)
  {
    *this = A;
  }

  /**
   * Overloading of the assignment operator
   * @param A The agent that is going to be copied inside the object
   * @return A pointer to the new copied Agent.
   */
  Agent & operator=(const Agent & A)
  {
    this->priority = A.getPriority();
    this->ID = A.getID();
    this->initPos = A.getInitPos();
    this->endPos = A.getEndPos();
    this->goals = A.getGoals();
    this->name = A.getName();
    this->solver = std::move(std::unique_ptr<SAPFSolver *>(A.solver.get()[0]->deepCopy()));
    this->path = A.getPath();
    return *this;
  }

  Agent deepCopy()
  {
    Agent A;
    A.setPriority(this->getPriority());
    A.setID(this->getID());
    A.setInitPos(this->getInitPos());
    A.setEndPos(this->getEndPos());
    A.setGoals(this->getGoals());
    A.setName(this->getName());
    A.setSolver(this->getSolver());
    A.setPath(this->path);
    return A;
  }

  /**
   * @return The priority of the agent
   */
  [[nodiscard]] int getPriority() const { return this->priority; }
  /**
   * @return The id of the agent
   */
  [[nodiscard]] int getID() const { return this->ID; }
  /**
   * @return The name of the agent
   */
  [[nodiscard]] std::string getName() const { return this->name; }
  /**
   * @brief Get the initial position of the agent
   * @return The initial position of the agent
   */
  [[nodiscard]] GraphNode getInitPos() const { return this->initPos; }
  /**
   * @brief Get the final position of the agent
   * @return The final position of the agent
   */
  [[nodiscard]] GraphNode getEndPos() const { return this->endPos; }
  /**
   * @return A vector containing the list of goals the agent should pass through
   */
  [[nodiscard]] std::vector<GraphNode> getGoals() const { return this->goals; }
  /**
   * @return A pointer to the SAPF solver used by the agent
   */
  [[nodiscard]] SAPFSolver * getSolver() { return this->solver.get()[0]; };
  /**
   * @return The path that the agent is going to follow
   */
  [[nodiscard]] std::vector<GraphNode> getPath() const { return this->path; }

  /**
   * @brief Sets the priority for the agent
   * @param _priority The new priority
   */
  void setPriority(int _priority) { this->priority = _priority; }
  /**
   * @brief Sets the ID for the agent
   * @param _id The new id for the agent
   */
  void setID(int _id) { this->ID = _id; }
  /**
   * @brief Sets the name for the agent
   * @param _name The new name
   */
  void setName(std::string _name) { this->name = std::move(_name); }
  /**
   * @brief Sets the initial position for the agent
   * @param _node The node being the new initial position
   */
  void setInitPos(GraphNode _node) { this->initPos = _node; }
  /**
   * @brief Sets the final position for the agent
   * @param _node The node being the new final position
   */
  void setEndPos(GraphNode _node) { this->endPos = _node; }
  /**
   * @brief Sets the list of goal positions for the agent
   * @param _goals A vector containing the new list of goal positions
   */
  void setGoals(std::vector<GraphNode> _goals) { this->goals = _goals; }
  /**
   * @brief Sets the new SAPF solver for the agent
   * @param _solver A pointer to the new SAPF solver to be used
   */
  void setSolver(SAPFSolver * _solver) { this->solver = std::make_unique<SAPFSolver *>(_solver); }
  /**
   * @brief Sets the path that the agent should follow
   * @param _path A vector containing the ordered list of nodes the agent will follow
   */
  void setPath(std::vector<GraphNode>& _path) { this->path = _path;}

  /**
   * @brief Basic function to solve the SAPF problem
   * @return A ordered list of nodes to solve the SAPF problem
   */
  std::vector<GraphNode> solve ()
  {
    std::vector<GraphNode> _path = this->getSolver()->solve(this->getInitPos(), this->getEndPos(), this->getGoals(), this->getID());
    this->setPath(_path);
    return _path;
  }

  /**
   * @brief Function to solve the SAPF problem for CBS since the algorithm will impose constraints on the
   * agent
   * @param constraints A list of constraints that the agent must respect
   * @return A ordered list of nodes to solve the SAPF problem respecting the list of constraints
   */
  std::vector<GraphNode> solveCBS (std::vector<Constraint>& constraints, std::vector<std::vector<GraphNode>> paths = {})
  {
    INFO(tprintf("Solving agent @", this->getID()));
    std::vector<GraphNode> _path = this->getSolver()->solveCBS(this->getInitPos(), this->getEndPos(), this->getGoals(), constraints, 0, this->getID(), paths);
    this->setPath(_path);
    return _path;
  }

  /**
   * @brief Function to check if two agents are equal
   * @param a The agent to compare
   * @return `true` if the two agents are the same, `false` otherwise
   */
  [[nodiscard]] inline bool equal(const Agent & a) const
  {
    return this->getID() == a.getID() && this->getName() == a.getName();
  }
  bool operator==(const Agent & a) const { return this->equal(a); }
  bool operator!=(const Agent & a) const { return !this->equal(a); }

  friend std::ostream & operator<<(std::ostream & out, const Agent & agent)
  {
    out << tprintf("[@] priority: @", agent.getID(), agent.getPriority());
    return out;
  }
};

#endif  //MAOF_AGENT_HPP
