#!/usr/bin/env/ python3

import json
import networkx as nx
import matplotlib.pyplot as plt

xMin = 1
xMax = 8
yMin = 31
yMax = 59
filename = "output/WH1/wh1"

G = nx.DiGraph()
# create nodes
nNodes = 0
nodes = []
labeldict = {}
for i in range(xMin,xMax+1):
    for j in range(yMin,yMax+1):
        string = "{:d}{:02d}".format(i, j)
        nodes.append([int(string), i, j, nNodes])
        print(nodes[-1])
        G.add_node(nNodes, pos = (j, i))
        labeldict[nNodes] = string
        nNodes += 1

print(nNodes)

# create connections
dY = yMax - yMin + 1
connect = [[0 for x in range(nNodes)] for x in range(nNodes)]
for i in range(xMin,xMax+1):
    idx = i-xMin
    for j in range(yMin,yMax+1):
        jdx = j-yMin
        nid = idx*dY + jdx
        print(nid, idx, jdx, i, j)
        connect[nid][nid] = 1
        G.add_edge(nid, nid)
        if jdx>0: 
            connect[nid][nid-1] = 1
            G.add_edge(nid, nid-1)
        if jdx<dY - 1:
            connect[nid][nid+1] = 1
            G.add_edge(nid, nid+1)
        if i<xMax and j == yMax:
            nextId = (idx+1)*dY + dY - 1
            connect[nextId][nid] = 1
            connect[nid][nextId] = 1
            G.add_edge(nextId, nid)
            G.add_edge(nid, nextId)
        if i < xMax and (j==yMin or j==yMin+1):
            nextId = (idx+1)*dY + jdx
            connect[nid][nextId] = 1
            connect[nextId][nid] = 1
            G.add_edge(nextId, nid)
            G.add_edge(nid, nextId)

labels = nx.get_edge_attributes(G,'weight')
plt.figure(figsize=(4096/90, 2160/90))
nx.draw_networkx_edge_labels(G,nx.get_node_attributes(G, 'pos'),edge_labels=labels)
nx.draw(G, nx.get_node_attributes(G, 'pos'), labels = labeldict, with_labels=True, node_size = 100, font_size = 3)
plt.gca().invert_yaxis()
plt.savefig(filename+".pdf", dpi = 1200, orientation = "landscape", transparent = False)
plt.savefig(filename+".png", dpi = 1200, orientation = "landscape", transparent = False)

with open("temp.json", "w+") as output:
    json.dump({
        "nNodes" : nNodes,
        "nodes" : nodes,
        "connect" : connect 
    }, output, indent = 2)

with open("temp.json", "r") as temp: 
    with open(filename+".json", "w+") as output:
        for line in temp:
            if any(x in line for x in ["{" , "}", "nNodes", "nodes", "connect", "]"]):
                output.write(line)
            else:
                line = line.split("\n")[0]
                output.write(line)

