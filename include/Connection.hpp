/**
 * @author Enrico Saccon (enrico.saccon@unitn.it)
 * @file Connection.hpp
 * @brief Class that models the connection between nodes of the environment.
 */

#ifndef MAOF_CONNECTION_HPP
#define MAOF_CONNECTION_HPP

// Library includes
#include <GraphNode.hpp>
#include <Utils.hpp>

// System includes
#include <algorithm>
#include <vector>

class Constraint;
typedef std::pair<size_t, int> TM_ID; ///< Type used to store the time and the id of an agent.

inline auto find_times = [](const std::vector<TM_ID> & times, size_t t) -> std::vector<TM_ID>::const_iterator
{
  return std::find_if(times.begin(), times.end(), [t](const TM_ID & p)->bool { return p.first == t; });
};

inline auto find_times_id = [](const std::vector<TM_ID> & times, const TM_ID & p) -> std::vector<TM_ID>::const_iterator
{
  return std::find(times.begin(), times.end(), p);
};

/**
 * @brief Class used to model the connection between two vertexes on a graph.
 * @details It can either be:
 *  - `ZERO`: when the connection is always limited
 *  - ONE: when the connection is always available
 *  - LIMIT_ONCE: when the connection is always available except for some times.
 *  - LIMIT_ALWAYS: when the connection is never available except for some times.
 *  The vector of times stores pairs (time, id) so that a connection is available (or not) w.r.t.
 *  an agent. If id is a negative value, then the connection is available (or not) to all the agents
 *  for that given time.
 */
class Connection
{
public:
  enum class TYPE { ZERO, ONE, LIMIT_ONCE, LIMIT_ALWAYS }; ///< The type of the connection

private:
  TYPE type;
  size_t value;
  std::vector<TM_ID> times;

public:
  /**
   * @brief Default constructor which initializes an empty Connection class
   */
  Connection() : type(TYPE::ZERO), value(0), times({}) {}
  /**
   * @brief Constructor that takes only the type, useful for when the connection is either `ZERO` or
   * ONE
   * @param _type The type of the connection as a Connection::TYPE value
   */
  Connection(TYPE _type, size_t _value = INF<size_t>()) : type(_type) {
    if (value == INF<size_t>()) {
      WARNING("Auto intialization of connection cost.")
      switch (_type) {
        case TYPE::ONE: {
          this->value = 1;
          break;
        }
        case TYPE::ZERO: {
          this->value = 0;
          break;
        }
        default:
          ERROR("Cannot initialize type LIMIT_ONCE or LIMIT_ALWAYS without value");
      }
    }
    else {
      this->value = _value;
    }
    times = {};
  }
  /**
   * Constructor that takes both the type of the connection and a vector of times
   * @param _type The type of the connection as a Connection::TYPE value
   * @param _times A vector of times in which the connection is or isn't available for a given agent
   */
  Connection(TYPE _type, size_t _value, std::vector<TM_ID> _times) : type(_type), value(_value), times(_times) {}
  /**
   * Constructor that takes both the type of the connection and a vector of times
   * @param _type The type of the connection as a Connection::TYPE value
   * @param _times A vector of times in which the connection is or isn't available
   */
  Connection(TYPE _type, size_t _value, std::vector<size_t> _times) : type(_type), value(_value)
  {
    for (size_t t : _times)
    {
      this->times.push_back(std::make_pair(t,-1));
    }
  }

  /**
   * @brief Get original value
   * @return The value of the connection WITHOUT considering its type.
   */
  size_t getCost() const { return this->value; }
  /**
   * @brief Set original value
   * @param _value The value of the connection WITHOUT considering its type.
   */
  void setCost(size_t _value) { this->value = _value; }
  /**
   * @brief Get the type of the connection
   * @return The type of the connection as a Connection::TYPE value
   */
  TYPE getType() const { return this->type; }
  /**
   * @brief Set the type of the connection
   * @param _type The type of the connection to set as a Connection::TYPE value
   */
  void setType(TYPE _type) {
    TYPE old_type = this->type;
    this->type = _type;
    if (this->getCost() == 0 && old_type==TYPE::ZERO && _type==TYPE::ONE) {
      WARNING("Auto intialization of connection cost.")
      this->setCost(1);
    }
    else if (this->getCost() > 0 && old_type==TYPE::ONE && _type==TYPE::ZERO) {
      WARNING("Auto intialization of connection cost.")
      this->setCost(0);
    }
  }

  /**
   * @brief Get the dimension of the vector of times
   * @return The size of the vector containing the timestamps
   */
  size_t size() const { return this->times.size(); }
  /**
   * @brief Overloading of operator [] to access an element in the timestamps
   * @details Notice that this function is marked as const, hence it cannot be used to modify the
   * value at a given position.
   * @param pos The position to access
   * @return The pair (time, id) in the position pos of the time vector
   */
  TM_ID operator[](int pos) const { return this->times[pos]; }

  /**
   * @brief Function to get the vector of time-ids
   * @return The vector containing the pairs time,id
   */
  std::vector<TM_ID> getTimes() const { return this->times; }

  /**
   * @brief Overloading of operator equality to check whether or not two Connections are the same.
   * @param con The connection which is going to be compared to
   * @return `true` if the two connections have the same type and also the same vector of time-ids,
   * `false` otherwise.
   */
  bool operator==(const Connection & con) const
  {
    return this->getType() == con.getType() && this->getTimes() == con.getTimes();
  }

  /**
   * @brief Overloading of operator different to check whether or not two Connections are the same.
   * @param con The connection which is going to be compared to
   * @return `false` if the two connections have the same type and also the same vector of time-ids,
   * `true` otherwise.
   */
  bool operator!=(const Connection & con) const { return !(*this == con); }

  /**
   * @brief Adds a new timestamp to the vector if it's not already present inside the vector
   * @details If the type of the connection is:
   * - `ONE`: it changes the type to `LIMIT_ONCE` and adds the time to the vector
   * - `ZERO`: it prints an error message and does nothing
   * - `LIMIT_ONCE`: it adds the time to the vector if it's not already present
   * - `LIMIT_ALWAYS`: it adds the time to the vector if it's not already present
   * @param t The time to insert
   * @param id The id of the agent using the edge at time t. Default is -1, that is "all agents"
   */
  void addTime(size_t t, int id = -1)
  {
    if (this->getType() == TYPE::ONE) {
      this->setType(TYPE::LIMIT_ONCE);
      this->times.push_back(std::make_pair(t, id));
    } else if (this->getType() == TYPE::ZERO) {
      WARNING("It is not possible to add a time to a ZERO connection, something may be wrong");
    } else {
      if (find_times(this->times, t) == this->times.end()) {
        this->times.push_back(std::make_pair(t, id));
      }
    }
  }

  /**
   * @brief Adds a new pair (timestamp, agent_id) to the vector if it's not already present inside
   * the vector
   * @details If the type of the connection is:
   * - `ONE`: it changes the type to `LIMIT_ONCE` and adds the time to the vector
   * - `ZERO`: it prints an error message and does nothing
   * - `LIMIT_ONCE`: it adds the time to the vector if it's not already present
   * - `LIMIT_ALWAYS`: it adds the time to the vector if it's not already present
   * @param t The time to insert
   * @param id The id of the agent using the edge at time t. Default is -1, that is "all agents"
   */
  void addTime(TM_ID tmId)
  {
    if (this->getType() == TYPE::ONE) {
      this->setType(TYPE::LIMIT_ONCE);
      this->times.push_back(tmId);
    } else if (this->getType() == TYPE::ZERO) {
      WARNING("It is not possible to add a time to a ZERO connection, something may be wrong");
    } else {
      if (find_times_id(this->times, tmId) == this->times.end()) {
        this->times.push_back(tmId);
      }
    }
  }

  /**
   * @brief Remove a time from the vector, independently from the agent associated.
   *
   * @details If the timestamp removed from the vector is actually the last one in the vector, then
   * the type of the connection is changed depending on the values:
   * - `LIMIT_ALWAYS` -> `ZERO`: since there are no more timestamps in which the connection is actually
   * active;
   * - `LIMIT_ONCE` -> `ONCE`: since there are no more timestamps in which the connection is unavailable
   *
   * @param t The time to be removed
   */
  void removeTime(size_t t)
  {
    auto it = find_times(this->times, t);
    while (it != this->times.end()){
      this->times.erase(it);
      if (this->times.empty()){
        if (this->getType() == TYPE::LIMIT_ALWAYS){
          this->setType(TYPE::ZERO);
        }
        else if (this->getType() == TYPE::LIMIT_ONCE){
          this->setType(TYPE::ONE);
        }
        else {
          std::cerr << "ERROR This point should not be reachable." << std::endl;
        }
      }
      it = find_times(this->times, t);
    }
  }

  /**
   * @brief Remove a pair (time, id) from the vector
   * @details If the timestamp removed from the vector is actually the last one in the vector, then
   * the type of the connection is changed depending on the values:
   * - `LIMIT_ALWAYS` -> `ZERO`: since there are no more timestamps in which the connection is actually
   * active;
   * - `LIMIT_ONCE` -> `ONCE`: since there are no more timestamps in which the connection is unavailable
   *
   * @param t The time to be removed
   */
  void removeTime(TM_ID p)
  {
    auto it = find_times_id(this->times, p);
    if (it != this->times.end()) {
      this->times.erase(it);
    }
    if (this->times.empty()){
      if (this->getType() == TYPE::LIMIT_ALWAYS){
        this->setType(TYPE::ZERO);
      }
      else if (this->getType() == TYPE::LIMIT_ONCE){
        this->setType(TYPE::ONE);
      }
      else {
        std::cerr << "ERROR This point should not be reachable." << std::endl;
      }
    }
  }

  /**
   * @brief Returns the cost of the connection
   * @details Checks if for a given moment t, the connection is free, independently of the agent.
   * Also if the time is the default (infinity), then the cost for `LIMIT_ONCE` and `LIMIT_ALWAYS`
   * is the cost of the connection and 0, respectively.
   * @param t A timestamp to check if the connection is available
   * @return An integer value
   * - the cost of the connection if it has type ONE, or if it has type `LIMIT_ONCE` and `t` is not in the
   * timestamps, or if it has type `LIMIT_ALWAYS` and `t` is in the timestamps
   * - 0 if the connection has type `ZERO`, or if it has type `LIMIT_ONCE` and t is in the timestamps,
   * or if it has type `LIMIT_ALWAYS` and t is not in the timestamps
   */
  size_t cost(size_t t = std::numeric_limits<size_t>::infinity()) const
  {
    switch (this->type) {
      case TYPE::ZERO: default:
        return 0;
      case TYPE::ONE:
        return this->value;
      case TYPE::LIMIT_ONCE: {
        if (!this->times.empty() && find_times(this->times, t) != this->times.end()) {
          return 0;
        }
        return this->value;
      }
      case TYPE::LIMIT_ALWAYS: {
        if (
          !this->times.empty() && find_times(this->times, t) != this->times.end()) {
          return this->value;
        }
        return 0;
      }
    }
  }

  /**
   * @brief Returns the cost of the connection and the id of the agent using it.
   * @param t A timestamp to check if the connection is available
   * @return A pair (cost, id)
   * - (value,0) if the connection has type ONE, or if it has type `LIMIT_ONCE` and t is not in the
   * timestamps, or if it has type `LIMIT_ALWAYS` and t is in the timestamps
   * - (0,id) if it has type `LIMIT_ONCE` and t is in the timestamps, or if it has type
   * `LIMIT_ALWAYS` and t is not in the timestamps
   * - (value,id) if the connection is of type `LIMIT_ALWAYS` and t is in the vector, meaning that
   * at that time the connection is available. Even though it returns also the id, it's up to the
   * programmer to know if the id has a meaning in their algorithm or not.
   * - (0,0) the connection has type `ZERO`;
   */
  TM_ID costId(size_t t = std::numeric_limits<size_t>::infinity()) const
  {
    switch (this->type) {
      case TYPE::ZERO: default: 
        return std::make_pair(0,0);
      case TYPE::ONE:
        return std::make_pair(this->value,0);
      case TYPE::LIMIT_ONCE: {
        // When the time is inside the vector, the connection should be dropped.
        if (!this->times.empty())
        {
          auto p = find_times(this->times, t);
          if (p != this->times.end())
          {
            return std::make_pair(0, p->second);
          }
        }
        return std::make_pair(this->value,0);
      }
      case TYPE::LIMIT_ALWAYS: {
        // Then one the time is inside the vector, the connection can be established
        if (!this->times.empty())
        {
          auto p = find_times(this->times, t);
          if (p != this->times.end())
          {
            return std::make_pair(this->value, p->second);
          }
          else {
            return std::make_pair(0, p->second);
          }
        }
        return std::make_pair(0, 0);
      }
    }
  }

  /**
   * @brief Function that returns a string containing information about the connection.
   * @param verbose If false, then it will only store the type of the connection, otherwise also the
   * times and ids of agents able/not able to use that connection.
   * @return A string describing the object.
   */
  [[nodiscard]] std::string toString(bool verbose = true) const
  {
    std::stringstream stream;
    switch (this->getType()){
      case TYPE::ZERO: {
        stream << "0";
        break;
      }
      case TYPE::ONE: {
        stream << "1";
        break;
      }
      case TYPE::LIMIT_ONCE: {
        stream << "L";
        break;
      }
      case TYPE::LIMIT_ALWAYS: {
        stream << "N";
        break;
      }
    }
    if (verbose &&
        (this->getType() == Connection::TYPE::LIMIT_ONCE ||
        this->getType() == Connection::TYPE::LIMIT_ALWAYS)) {
      stream << ", {";
      for (size_t i = 0; i < this->size(); i++) {
        stream << tprintf("(@,@)", this->times[i].first, this->times[i].second); // First is time, secondo is ID
        if (i != this->size() - 1) {
          stream << " ";
        }
      }
      stream << "}";
    }
    return stream.str();
  }

  friend std::ostream & operator<<(std::ostream & stream, const Connection & c)
  {
    stream << c.toString();
    return stream;
  }
};

/**
 * @brief Function used to convert an integer connectivity matrix in a matrix of Connection types
 * @param nodes The list of nodes
 * @param connect The integer connectivity matrix
 * @return The connectivity matrix using Connection types
 */
std::vector<std::vector<Connection>> convertConnect(const std::vector<std::vector<int>> & connect);



#endif  //MAOF_CONNECTION_HPP