import re

from . import Stat
from . import Num

class CrossStat:
    def __init__(self, r1, r2):
        self.values = dict()
        self.r1 = r1
        self.r2 = r2

    def parse(self, string):
        m2 = re.search(self.r2, string)
        if m2:
            if m2["val"] in self.values.keys():
                self.values[m2["val"]].parse(string)
            else:
                self.values[m2["val"]] = Stat.Stat(self.r1)
                self.values[m2["val"]].parse(string)
        else:
            return False

    def __str__(self):
        string = "{\n"
        for key in self.values:
            string += "  {}, {}\n".format(key, str(self.values[key]))
        string += "}"
        return string

    def toLatexTable(self):
        string = "\\begin{tabular}{"
        for i in range(len(self.values.keys())):
            string += "c"
            if i < len(self.values.keys())-1:
                string += "|"
        string += "}\n\t"
        vals = sorted(self.values)
        self.values.values
        for val in vals.values:
            string += val.main()

        string += "\n\\end{tabular}"

class CrossNum:
    def __init__(self, r1, r2):
        self.values = dict()
        self.r1 = r1
        self.r2 = r2

    def parse(self, string):
        m2 = re.search(self.r2, string)
        if m2:
            if m2["val"] in self.values.keys():
                self.values[m2["val"]].parse(string)
            else:
                self.values[m2["val"]] = Num.Num(self.r1)
                self.values[m2["val"]].parse(string)
        else:
            return False

    def __str__(self):
        string = "{\n"
        for key in self.values:
            string += "  {}, {}\n".format(key, str(self.values[key]))
        string += "}"
        return string
