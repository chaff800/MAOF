/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file Conflict.hpp
 * @brief 
 */

#ifndef MAOF_CONFLICT_HPP
#define MAOF_CONFLICT_HPP

// Library includes
#include "Agent.hpp"


class Conflict {
public:
  enum TYPE { NONE, VERTEX, SWAP };
private:
  Conflict::TYPE type;
  std::vector<size_t> agentsIds;
  std::vector<GraphNode> nodes;
  size_t time;

public:
  Conflict() : type(Conflict::NONE), agentsIds({}), nodes({}), time(0) {}

  Conflict(Conflict::TYPE _type, std::vector<size_t> _agentsIds, std::vector<GraphNode> _nodes, size_t _time) :
    type(_type), agentsIds(_agentsIds), nodes(_nodes), time(_time) {}

  // Getters and setters
  Conflict::TYPE getType() const { return type; }
  std::vector<size_t> getAgents() const { return agentsIds; }
  size_t getAgent(size_t i) const { return agentsIds.at(i); }
  std::vector<GraphNode> getNodes() const { return nodes; }
  GraphNode getNode(size_t i) const { return nodes.at(i); }
  size_t getTime() const { return time; }

  void setType(Conflict::TYPE _type) { type = _type; }
  void setAgents(const std::vector<size_t>& _agentsIds) { agentsIds = _agentsIds; }
  void setNodes(const std::vector<GraphNode>& _nodes) { nodes = _nodes; }
  void setTime(size_t _time) { time = _time; }

  bool isNone () const { return type == Conflict::NONE; }

  std::string out() const {
    std::stringstream stream;
    stream << "Type: ";
    switch (type) {
      case VERTEX: stream << "VERTEX"; break;
      case SWAP: stream << "SWAP"; break;
      case NONE: default: stream << "NONE"; break;
    }
    stream << std::endl;
    stream << tprintf("Time: @ Agents: @ nodes @", this->time, this->agentsIds, this->nodes) << std::endl;
    return stream.str();
  }

  friend std::ostream& operator<<(std::ostream& os, const Conflict& conflict) {
    os << conflict.out();
    return os;
  }
};

Conflict findConflict (const std::vector<Agent> & agents);
Conflict findConflict (const std::vector<std::vector<GraphNode>> & paths);

#endif  //MAOF_CONFLICT_HPP
