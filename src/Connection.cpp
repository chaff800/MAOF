/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @date 27/07/2022
 * @file Connection.cpp
 */

#include <Connection.hpp>
#include <MAPF/Constraint.hpp>

std::vector<std::vector<Connection>> convertConnect(const std::vector<std::vector<int>> & connect)
{
  std::vector<std::vector<Connection> > newConnect(connect.size());
  for (size_t i = 0; i < connect.size(); i++) {
    newConnect[i].resize(connect[i].size());
    for (size_t j = 0; j < connect[i].size(); j++) {
      newConnect[i][j] = Connection((connect[i][j] ? Connection::TYPE::ONE : Connection::TYPE::ZERO), connect[i][j]);
    }
  }
  return newConnect;
}

