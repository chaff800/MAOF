FROM chaff800/cmake_pipeline:latest

COPY . /tmp

WORKDIR /tmp

RUN cd testingScripts/generate && python3 -m pip install -r requirements.txt && cd shortestPath && python3 createSPTests.py && cd /tmp
RUN if [ -d build ]; then rm -rf build; fi; mkdir build
RUN cmake -DCMAKE_BUILD_TYPE=Test -B build -S .
RUN cmake --build build -- -j

CMD ctest --test-dir build