#!/usr/bin/env/ python3

import json
import networkx as nx
import matplotlib.pyplot as plt
from Node import Node

xMin = 17
xMax = 23
yMin = 1
yMax = 13
filename = "output/WH2_1/wh2_1"

G = nx.DiGraph()
# create nodes
nNodes = 0
nodes = []
labeldict = {}
for i in range(xMin,xMax+1):
    for j in range(yMin,yMax+1):
        string = "{:d}{:02d}".format(i, j)
        nodes.append(Node(int(string), nNodes, i, j))
        print(nodes[-1])
        G.add_node(nNodes, pos = (j, i))
        labeldict[nNodes] = string
        nNodes += 1

print(nNodes)

# create connections
connect = [[0 for x in range(nNodes)] for x in range(nNodes)]
for i in range(xMin,xMax+1):
    idx = i-xMin
    for j in range(yMin,yMax+1):
        jdx = j-yMin
        nid = idx*yMax + jdx
        print(nid, idx, jdx, i, j)
        connect[nid][nid] = 1
        G.add_edge(nid, nid)
        if jdx>0: 
            connect[nid][nid-1] = 1
            G.add_edge(nid, nid-1)
        if jdx<yMax - 1:
            connect[nid][nid+1] = 1
            G.add_edge(nid, nid+1)
        if i<xMax and j == yMax:
            nextId = (idx+1)*yMax + yMax - 1
            connect[(idx+1)*yMax + yMax - 1][nid] = 1
            connect[nid][(idx+1)*yMax + yMax - 1] = 1
            G.add_edge((idx+1)*yMax + yMax - 1, nid)
            G.add_edge(nid, (idx+1)*yMax + yMax - 1)

nx.draw(G, nx.get_node_attributes(G, 'pos'), labels = labeldict, with_labels=True, node_size = 100, font_size = 3)
plt.gca().invert_yaxis()
# plt.show()
plt.savefig(filename+".pdf", dpi = 1200, orientation = "landscape", transparent = False)
plt.savefig(filename+".png", dpi = 1200, orientation = "landscape", transparent = False)

with open("temp.json", "w+") as output:
    json.dump({
        "nNodes" : nNodes,
        "nodes" : [n.toList() for n in nodes],
        "connect" : connect 
    }, output, indent = 2)

with open("temp.json", "r") as temp: 
    with open(filename+".json", "w+") as output:
        for line in temp:
            if any(x in line for x in ["{" , "}", "nNodes", "nodes", "connect", "]"]):
                output.write(line)
            else:
                line = line.split("\n")[0]
                output.write(line)

