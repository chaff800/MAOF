import plotly.graph_objs as go
import re
from math import log
import matplotlib.pyplot as plt

import plotly.io as pio
pio.kaleido.scope.mathjax = None

time_re = r"time: min \d+\.?\d* avg \d+\.?\d* max \d+\.?\d* counter \d+ values: \[(?P<list>(\d+.?\d* ?)*)\]"

labels = {
    "ICTS" : "a",
    "CBS" : "b",
    "CP" : "c",
    "ICR+CP" : "d",
    "ICR+CBS" : "e"
}

def mainPlotly(dirs, output):
    times = {
        "ICTS" : [],
        "CBS" : [],
        "CP" : [],
        "ICR+CP" : [],
        "ICR+CBS" : []
    }

    fig = go.Figure()
    maxTime = 0
    totalTests = "48"
    for key in dirs.keys():
        if "WH" in dirs[key]:
            totalTests = "108"
        val = dirs[key]
        if val == "":
            continue
        with open(val) as f:
            text = f.read()
            match = re.search(time_re, text)
            if match:
                tmp_list = match.group("list")
                for val in tmp_list.split(", "):
                    times[key].append(float(val)/1000.0)
                times[key].sort()
            maxTime = max(times[key]) if max(times[key]) > maxTime else maxTime

        fig.add_trace(go.Scatter(x=list(range(len(times[key]))), y=times[key], name=key, line=dict(shape="linear", width=2), mode="lines+markers"))

        for i in range(0, len(times[key])):
            print("{} {} {}".format(i, times[key][i], labels[key]))

#            ['linear', 'spline', 'hv', 'vh', 'hvh', 'vhv']

    fig.update_xaxes(showline=True, linewidth=2, linecolor='Black', mirror=False, gridcolor='LightGray', gridwidth=1, zeroline=True, zerolinecolor="LightGray")
    fig.update_yaxes(showline=True, linewidth=2, linecolor='Black', mirror=False, gridcolor='LightGray', gridwidth=1, zeroline=True, zerolinecolor="Gray")
    fig.update_layout(
        xaxis_title="Number of solutions out of {} tests".format(totalTests),
        yaxis_title="Time [s]",
        paper_bgcolor="rgba(0,0,0,0)",
        plot_bgcolor="rgba(0,0,0,0)",
        xaxis_title_font_size=28,
        yaxis_title_font_size=28,
        yaxis = dict(
            tickmode = "linear",
            tick0 = 0,
            dtick = 10 if maxTime > 10 else 1,
            # tickvals = [0, 10, 20, 30, 40, 48],
        ),
        font_family = "Roman-serif",
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1,
            xanchor="right",
            x=0.8,
            font_size = 20
        )
    )

    # fig.show()
    fig.write_image(output, width=800, height=500, scale=10)


if __name__ == "__main__":
    dirs = {
        "ICTS" : "/home/enrico/Desktop/maof_tests/test/outputUni/2023-03-08_17.01.23_ICTS_10/log.log",
        "CBS" : "/home/enrico/Desktop/maof_tests/test/outputUni/2023-03-08_17.11.34_CBS_10/log.log",
        "CP" : "./outputUni/2023-03-08_23.58.04_CP_10/log.log",
        "ICR+CP" : "./outputUni/2023-03-09_00.04.04_ICR_CP_10/log.log",
        "ICR+CBS" : "/home/enrico/Desktop/maof_tests/test/outputUni/2023-03-08_17.27.54_ICR_CBS_10/log.log"
    }
    mainPlotly(dirs, "/home/enrico/Downloads/newplotPovo10s.png")

    dirs = {
        "ICTS" : "/home/enrico/Desktop/maof_tests/test/outputWH/2023-03-08_19.54.44_ICTS_10/log.log",
        "CBS" : "/home/enrico/Desktop/maof_tests/test/outputWH/2023-03-08_19.28.10_CBS_10/log.log",
        "CP" : "./outputWH/2023-03-09_01.10.50_CP_10/log.log",
        "ICR+CP" : "./outputWH/2023-03-09_01.25.36_ICR_CP_10/log.log",
        "ICR+CBS" : "/home/enrico/Desktop/maof_tests/test/outputWH/2023-03-08_19.40.39_ICR_CBS_10/log.log"
    }
    mainPlotly(dirs, "/home/enrico/Downloads/newplotWH10s.png" )

    dirs = {
        "ICTS" : "/home/enrico/Desktop/maof_tests/test/outputUni/2023-03-08_18.16.42_ICTS_60/log.log",
        "CBS" : "/home/enrico/Desktop/maof_tests/test/outputUni/2023-03-08_17.31.54_CBS_60/log.log",
        "CP" : "./outputUni/2023-03-09_00.08.02_CP_60/log.log",
        "ICR+CP" : "./outputUni/2023-03-09_00.42.48_ICR_CP_60/log.log",
        "ICR+CBS" : "/home/enrico/Desktop/maof_tests/test/outputUni/2023-03-08_17.54.16_ICR_CBS_60/log.log"
    }
    mainPlotly(dirs, "/home/enrico/Downloads/newplotPovo60s.png")

    dirs = {
        "ICTS" : "/home/enrico/Desktop/maof_tests/test/outputWH/2023-03-09_09.50.38_ICTS_60/log.log",
        "CBS" : "./outputWH/2023-03-09_03.26.21_CBS_60/log.log",
        "CP" : "./outputWH/2023-03-09_04.24.22_CP_60/log.log",
        "ICR+CP" : "./outputWH/2023-03-09_05.49.36_ICR_CP_60/log.log",
        "ICR+CBS" : "./outputWH/2023-03-09_07.00.20_ICR_CBS_60/log.log"
    }
    mainPlotly(dirs, "/home/enrico/Downloads/newplotWH60s.png")
