/**
* @author Enrico Saccon <enrico.saccon@unitn.it>
* @file CBS.cpp
* @brief Implementation of the CBS class
*/

#include "MAPF/CBS/CBS.hpp"

#include <cstdlib>

typedef std::vector<GraphNode> Path;

/**
 * Function to create a dot file of the Constraint Tree (CT).
 * @param filename The filename of the dot file and the PDF.
 * @param root The root of the CT from which a BFS starts.
 */
static void create_dot_file(std::string filename, std::shared_ptr<CBSNode> root) {
  std::ofstream dot_file(filename+".dot");
  dot_file << "digraph G {" << std::endl << "# NODES" << std::endl;

  //BFS
  std::deque<std::shared_ptr<CBSNode>> openList = {root};
  while (!openList.empty()) {
    std::shared_ptr<CBSNode> current = openList.front();
    for (auto & child : current->getChildren()) {
      openList.push_back(child);
    }
    dot_file << tprintf("\tn@ [label=\"@\n@\n@\"];\n", current.get(), current->getCost(), printTablePaths(current->getSolution()), current->getConstraints());
    openList.pop_front();
  }

  dot_file << "#EDGES" << std::endl;

  openList = {root};

  while (!openList.empty()) {
    std::shared_ptr<CBSNode> current = openList.front();
    for (auto & child : current->getChildren()) {
      openList.push_back(child);
    }
    dot_file << tprintf("\tn@ -> n@;\n", current->getParent(), current.get());
    openList.pop_front();
  }

  dot_file << "}" << std::endl;
  dot_file.close();

  system((std::string("dot -Tpdf ")+filename+".dot > "+filename+".pdf").c_str());
}

/**************************************************************************************************/

std::vector<Path> CBSSolver::solve()
{
  // Initialize the constraint tree
  std::shared_ptr<CBSNode> root (new CBSNode());
  this->lowLevelAgents(root);

  std::priority_queue<std::shared_ptr<CBSNode>, std::vector<std::shared_ptr<CBSNode>>, std::greater<std::shared_ptr<CBSNode>>> openList;
  openList.push(root);
  bool solved = false;

  size_t counter = 0;
  std::shared_ptr<CBSNode> current;
  do {
    counter ++;
    current = openList.top(); openList.pop();

    INFO_COL(tprintf("Checking CBS node with cost @ for conflicts\n@", current->getCost(), current->getSolution()), Color::BLUE);
    Conflict conflict = findConflict(current->getSolution());
    // If the solution does not have conflicts, then we are done
    if (conflict.isNone()){
      solved = true;
      continue;
    }
    INFO_COL(tprintf("Found conflict @", conflict), Color::BLUE);

    for (Constraint& c : getConstraintsFromConflicts(conflict)){
      // Create a new node
      std::shared_ptr<CBSNode> newNode (new CBSNode(current, current->getConstraints(), current->getSolution(), -1));

      // Compute the paths with the new constraint
      std::vector<Path> paths = this->lowLevelAgentsSingle(newNode, c);

      INFO_COL(tprintf("\n\n\nExploring CBS node with constraints @", newNode->getConstraints()), Color::CYAN);

      // If the solution is valid, then add the new node to the priority queue
      if (!paths.empty()) {
        INFO_COL(tprintf("Adding CBS node with cost @ to openSet\n@", newNode->getCost(), newNode->getSolution()), Color::CYAN);
        openList.push(newNode);
        current->addChild(newNode);
      }
    }
  }
  while (!openList.empty() && !solved);

  std::vector<std::vector<GraphNode>> sol = current->getSolution();

  PARSER("Expanded @ CBSnodes", counter);
  return sol;
}

/**************************************************************************************************/

std::vector<Path> CBSSolver::lowLevelAgents(std::shared_ptr<CBSNode> & node){
  std::vector<Path> paths;
  std::vector<Constraint> constraints = node->getConstraints();
  for (size_t i = 0; i < this->agents.size(); i++){
    Path tmp = this->agents[i].solve();

    if (!tmp.empty()){
      paths.push_back(tmp);
    }
    else {
      INFO(tprintf("Could not compute a feasible path for agent @ with conflicts @", this->agents[i].getID(), constraints));
      return {};
    }
  }
  node->setSolution(paths);
  node->setCost(this->cost(paths));
  return paths;
}

/**************************************************************************************************/

std::vector<Path> CBSSolver::lowLevelAgentsSingle(std::shared_ptr<CBSNode> & node, const Constraint & c){
  node->addConstraint(c);
  std::vector<Constraint> allConstraints = node->getConstraints();
  std::vector<Constraint> constraints;
  std::copy_if(allConstraints.begin(), allConstraints.end(), std::back_inserter(constraints),
               [c](const Constraint & constraint) { return constraint.getAId() == c.getAId(); });

  std::vector<Path> oldPaths = node->getSolution();
  oldPaths.erase(std::next(oldPaths.begin(), c.getAId()));
  Path path = this->agents[c.getAId()].solveCBS(constraints, oldPaths);
  if (path.empty()){
    INFO(tprintf("Could not compute a feasible path for agent @ with conflicts @", this->agents[c.getAId()].getID(), constraints));
    return {};
  }

  node->setSolution(path, c.getAId());
  node->setCost(this->cost(node->getSolution()));

  return node->getSolution();
}

/**************************************************************************************************/

void CBSSolver::postProcess(std::vector<std::vector<GraphNode>> & paths)
{
  std::vector<std::vector<GraphNode>> backup = paths;

  size_t maxLen = std::max_element(paths.begin(), paths.end(),
                                   [](auto & path1, auto & path2)
                                   { return path1.size() < path2.size(); })->size();
  // Pad paths
  for (std::vector<GraphNode> & path : backup){
    std::cout << path << std::endl;
    for (size_t i = path.size(); i < maxLen; i++){
      path.push_back(path.back());
    }
  }

  std::vector<bool> reduced (backup.size(), false);

  for (size_t pathId = 0; pathId<backup.size(); pathId++){
    for (size_t i = 1; i<backup[pathId].size()-1; i++) {
      if (backup[pathId][i-1] == backup[pathId][i]){
        bool remove = true;
        // Test removing path[i] does not create vertex conflicts
        for (size_t j = 0; j<backup.size() && remove; j++){
          if (pathId != j && backup[pathId][i+1] == backup[j][i]){
            remove = false;
          }
        }
        // Test removing path[i] does not create a swap conflict
        for (size_t j = 0; j<backup.size() && remove; j++){
          if (pathId != j && backup[pathId][i+1] == backup[j][i-1] && backup[pathId][i-1] == backup[j][i+1]){
            remove = false;
          }
        }
        if (remove){
          backup[pathId].erase(std::next(backup[pathId].begin(), i));
          reduced[pathId] = true;
        }
      }
    }
  }

  if (findConflict(backup).isNone()) { paths = backup; }
}

/**************************************************************************************************/

std::vector<std::vector<GraphNode>> CBSSolver::solveICR(const std::vector<size_t>& times){
  if (times.size() != this->agents.size()){
    ERROR("The number of times does not match the number of agents");
  }
  for (size_t i=0; i<times.size(); i++){
    // Add as many goals as the times the agent should wait in its start position
    std::vector<GraphNode> _goals (times[i], this->agents[i].getInitPos());
    std::vector<GraphNode> oldGoals = this->agents[i].getGoals();
    _goals.insert(_goals.end(), oldGoals.begin(), oldGoals.end());
    this->agents[i].setGoals(_goals);
  }
  INFO(tprintf("NEW\n@", this->out()));
  return this->solve();
}