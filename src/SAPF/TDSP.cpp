/*!
 * @author Enrico Saccon
 * @file TDSP.cpp
 * @brief File that contains functions to solve the SAPF problem using TDSP.
 */
#include "SAPF/TDSP/TDSP.hpp"

static bool TDSPMes = false;

static std::vector<GraphNode> backtrack(GraphNode initPos, GraphNode endPos, const std::vector<GraphNode> & q)
{
  std::vector<GraphNode> path = {endPos};

  size_t counter = 0;

  while (path.back() != initPos && counter < 2 * q.size()) {
    GraphNode next = q[path.back().getId()];
    if (next.getId() == 0 && next.getV() == 0)
    {
      size_t i = 0;
      for (auto n : q) {
        INFO(tprintf("@ @", i, n))
        i++;
      }
      ERROR("Backtracking: cannot find previous node");
    }
    path.push_back(next);
    counter++;
  }

  if (counter == 2 * q.size()) {
    size_t i = 0;
    for (auto n : q) {
      INFO(tprintf("@ @", i, n))
      i++;
    }
    ERROR("Backtracking: loop avoidance");
  }

  //  path.push_back(initPos);
  std::reverse(path.begin(), path.end());

  INFO("Found path from @ to @", initPos, endPos);

  return path;
}

/*--------------------------------------------------------------------------------------------------------------------*/

static std::vector<GraphNode> TDSPInitEnd(
  std::vector<GraphNode> & nodes, GraphNode pos, std::vector<GraphNode> & sol,
  const Graph * connect, size_t depTime = 0)
{
  sol.push_back(pos);
  // Function cost returns 0 if there is no connection, 1 if the connection is established and free to be accessed,
  // LIMIT_CONNECTION otherwise.
  if (connect->getCost(pos, pos, depTime+1)== 1) {
    return sol;
  } else {
    return TDSPInitEnd(nodes, pos, sol, connect, depTime + 1);
  }
}

/*--------------------------------------------------------------------------------------------------------------------*/
/**
 * @Brief Function to compute the shortest path between two points on a graph using TDSP
 * @param nodes A vector containing the nodes of the graph
 * @param initPos The initial vertex
 * @param endPos The final vertex
 * @param connect A connectivity matrix made SPTests of Connection types
 * @param a_id The id of the Agent which is being considered
 * @param depTime The initial time of departure (used with connect to check the cost of the edges)
 * @return The shortest path going from `initPos` to `endPos`
 */
static std::vector<GraphNode> TDSP(
  std::vector<GraphNode> & nodes, GraphNode initPos, GraphNode endPos,
  const Graph* connect, size_t a_id, size_t depTime = 1)
{
  FUNCTION_NAME("Using TDSP", TDSPMes)

  if (initPos == endPos) {
    std::vector<GraphNode> sol = {initPos};
    return sol;
//    return TDSPInitEnd(nodes, initPos, sol, connect, depTime);
  }

  // Keep track of distances from initial position and whether the node has been visited or not
  std::vector<std::pair<size_t, bool> > L(
    nodes.size(), std::make_pair(std::numeric_limits<size_t>::max(), false));

  // Keep backtracking vector
  std::vector<GraphNode> q(nodes.size(), GraphNode());

  L[initPos.getId()] = std::make_pair(depTime, false);

  // Store visited nodes
  std::vector<GraphNode> F;

  std::sort(
    nodes.begin(), nodes.end(), [](const GraphNode & a, const GraphNode & b) { return a.getId() < b.getId(); });

  while (true) {
    size_t pos = std::distance(
      L.begin(), std::min_element(
                   L.begin(), L.end(),
                   [](const std::pair<size_t, uint8_t> & a, const std::pair<size_t, uint8_t> & b) {
                     if (a.second) { // If the first node has already been visited, then return the value of the second node.
                       return (bool)b.second;
                     }
                     else { // If the first node has not been visited, then return the first only if the second has been visited or if the second has not been visited, but the first has a smaller distance.
                       if (b.second || (!b.second && a.first < b.first)) {
                         return true;
                       }
                       else {
                         return false;
                       }
                     }
                   }));
    F.push_back(nodes[pos]);
    L[pos].second = true;

    // Cycle through the other nodes in the list and check if any of them is reachable.
    for (size_t j = 0; j < nodes.size(); j++) {
      if (j != pos && connect->getCost(pos, j, L[pos].first)) {
        size_t tmp = L[pos].first + connect->getCost(pos, j, L[pos].first);
        if (tmp < L[j].first) {
          L[j].first = tmp;
          q[j] = nodes[pos];
        }
      }
    }

    // TODO create function to compare nodes and F
    std::sort(
      F.begin(), F.end(), [](const GraphNode & a, const GraphNode & b) { return a.getId() < b.getId(); });
    if (F == nodes) {
      break;
    }
  }

  std::vector<GraphNode> sol = backtrack(initPos, endPos, q);

  return sol;
}

/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * @brief A function that returns the shortest path for an agent going from its initial position to
 * its final position and through the different intermediate goal positions
 * @details We assume the order of the goals is already optimal, hence the problem reduced to
 * calling TDSP P2P for the adjacent couples of nodes in `[initPos, goalPos, endPos]`.
 *
 * @param nodes The list of nodes in the graph
 * @param initPos The initial position
 * @param endPos The arrival position
 * @param goalPos A list containing the intermediate position to meet
 * @param connect A connectivity matrix made SPTests of Connection classes
 * @param a_id The id of the agent that is being considered
 * @param depTime The initial departing time (used in combination with Connection to return the
 * correct cost)
 * @return The shortest path going from `initPos` to `endPos` meeting the different goal positions
 */
static std::vector<GraphNode> TDSP(
  std::vector<GraphNode> & nodes, GraphNode initPos, GraphNode endPos, std::vector<GraphNode> & goalPos,
  const Graph* connect, size_t a_id = 0, size_t depTime = 1)
{
  FUNCTION_NAME("Using TDSP", TDSPMes)

  if (goalPos.empty()) {
    return TDSP(nodes, initPos, endPos, connect, depTime);
  }

  std::vector<GraphNode> finalPath;
  GraphNode s = initPos;
  GraphNode f = goalPos[0];

  // Compute path from initPos to first goalPos
  std::vector<GraphNode> tmp = TDSP(nodes, s, f, connect, depTime);

  if (tmp.empty()) {
    std::cerr << "Error, TDSP could not find a path between " << s << " and " << f << std::endl;
    return {};
  }
  finalPath.insert(finalPath.end(), tmp.begin(), tmp.end());

  // Compute paths for intermediate goals
  for (size_t i = 1; i < goalPos.size(); i++) {
    s = f;
    f = goalPos[i];

    tmp = TDSP(nodes, s, f, connect, tmp.size());

    if (tmp.empty()) {
      std::cerr << "Error, TDSP could not find a path between " << s << " and " << f << std::endl;
      return {};
    }

    // If the path computed is just one node, then that's simply staying on a goal, which is fine
    // and should be preserved.
    finalPath.insert(finalPath.end(), tmp.begin() + (tmp.size() > 1 ? 1 : 0), tmp.end());
  }

  // Compute path from final goalPos to endPos
  s = f;
  f = endPos;

  tmp = TDSP(nodes, s, f, connect, tmp.size());

  if (tmp.empty()) {
    std::cerr << "Error, TDSP could not find a path between " << s << " and " << f << std::endl;
    return {};
  }
  finalPath.insert(finalPath.end(), tmp.begin() + 1, tmp.end());

  return finalPath;
}

/*--------------------------------------------------------------------------------------------------------------------*/

std::vector<GraphNode> TDSP(
  std::vector<GraphNode> nodes, GraphNode initPos, GraphNode endPos, std::vector<GraphNode> & goalPos,
  Graph * connect, std::vector<Constraint> & constraints, size_t a_id = 0)
{
  FUNCTION_NAME("Using TDSP", TDSPMes)
  std::vector<GraphNode> placeholders;

  connect->addConstraints(constraints, &placeholders);

  nodes = connect->getNodes();

  std::vector<GraphNode> sol = TDSP(nodes, initPos, endPos, goalPos, connect);

//  if (!placeholders.empty()) {
//    INFO("Fixing placeholders")
//    print_v(sol);
//    bool PHFound = false;
//    do {
//      PHFound = false;
//      // Go through all the nodes in the solution and check if any is a placeholder
//      for (size_t i = 0; i < sol.size(); i++) {
//        GraphNode n = sol[i];
//        auto it = std::find_if(
//          placeholders.begin(), placeholders.end(), [n](const auto & a) { return a.first == n; });
//        // If there is one, then save the index it is in as the last "safe location", remove it and add previous safe location.
//        if (it != placeholders.end()) {
//          sol[i] = sol[i - 1];
//          PHFound = true;
//        }
//      }
//    } while (PHFound);
//    print_v(sol);
//  }

  return sol;
}

std::vector<GraphNode> TDSPSolver::solve(GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals, size_t depTime, int a_id)
{
  std::vector<GraphNode> nodes = this->getGraph()->getNodes();
  return TDSP(nodes, initPos, endPos, goals, this->connect.get(), a_id, this->depTime);
}

std::vector<GraphNode> TDSPSolver::solveCBS(GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals, std::vector<Constraint> & constraints, size_t depTime, int a_id, std::vector<std::vector<GraphNode>> paths)
{
  std::vector<GraphNode> nodes = this->getGraph()->getNodes();
  return TDSP(nodes, initPos, endPos, goals, this->connect.get(), constraints, a_id);
}
