class Node:
    def __init__(self, _val, _id, _x, _y):
        self.val = _val
        self.id = _id
        self.x = _x
        self.y = _y

    def __getitem__(self, item):
        if item == -1 or item == 3:
            return self.id
        elif item == 0:
            return self.val
        elif item == 1:
            return self.x
        elif item == 2:
            return self.y
        else:
            raise Exception("Item {} out of range".format(item))

    def toList(self):
        return [self.val, self.id, self.x, self.y]

    def __str__(self):
        return str(self.toList())