/**
* @author Giovanni Lorenzini and Diego Planchenstainer
* @file NodeAgentLabel.hpp
 */

#ifndef MAOF_HASH_HPP
#define MAOF_HASH_HPP

#include <ankerl/unordered_dense.h>

/// @brief Combine an hash with a new value to be hashed.
/// @param seed hash to be combined
/// @param v value to be hashed
inline void hash_combine(size_t & seed, size_t v)
{
  const size_t kMul = 0x9ddfea08eb382d69ULL;
  size_t a = (ankerl::unordered_dense::detail::wyhash::hash(v) ^ seed) * kMul;
  a ^= (a >> 47);
  size_t b = (seed ^ a) * kMul;
  b ^= (b >> 47);
  seed = b * kMul;
}

#endif  // MAOF_HASH_HPP
