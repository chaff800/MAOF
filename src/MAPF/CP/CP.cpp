/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file CP.cpp
 * @brief 
 */

#include "MAPF/CP/CP.hpp"

// TODO I think that for how the paths are solved, MKS and SIC are the same, which is not correct.

std::vector<std::vector<GraphNode>> CPSolver::solveICR(const std::vector<size_t>& times) {
  if (times.size() != this->agents.size()){
    ERROR("The number of times does not match the number of agents");
  }
  for (size_t i=0; i<times.size(); i++){
    // Add as many goals as the times the agent should wait in its start position
    std::vector<GraphNode> _goals (times[i], this->agents[i].getInitPos());
    std::vector<GraphNode> oldGoals = this->agents[i].getGoals();
    _goals.insert(_goals.end(), oldGoals.begin(), oldGoals.end());
    this->agents[i].setGoals(_goals);
  }
  return this->solve();
}

// Function to pretty print a tree
void prettyPrintTree(const std::unique_ptr<TreeNode> & root, size_t depth)
{
  if (root == nullptr) {
    std::cout<< "Empty tree"<< std::endl;
  }

  std::cout << root->val << std::endl;
  if (root->left || root->right) {
    if (root->left) {
      std::cout << std::setw(2 * (depth)) << " ";
      std::cout << "├── ";
      prettyPrintTree(root->left, depth + 1);
    }
    if (root->right) {
      std::cout << std::setw(2 * (depth)) << " ";
      std::cout << "└── ";
      prettyPrintTree(root->right, depth + 1);
    }
  }
}

std::unique_ptr<TreeNode> constructBST(std::vector<size_t>& nums) {
  if (nums.empty()) {
    return nullptr;
  }

  int mid = (nums.size() - 1) / 2;
  std::unique_ptr<TreeNode> root;
  if (nums.size() % 2 == 0) {
    // Even number of elements
    root = std::make_unique<TreeNode>(nums[mid]);
  } else {
    // Odd number of elements
    root = std::make_unique<TreeNode>(nums[mid]);
  }
  std::vector<size_t> left(nums.begin(), nums.begin() + mid);
  std::vector<size_t> right(nums.begin() + mid + 1, nums.end());
  if (left.size() == 1) {
    root->left = std::make_unique<TreeNode>(left[0]);
  }
  else {
    root->left = constructBST(left);
  }
  if (right.size() == 1) {
    root->right = std::make_unique<TreeNode>(right[0]);
  }
  else {
    root->right = constructBST(right);
  }
  return root;
}

void CPSolver::_solve(std::vector<std::vector<GraphNode>>& paths)
{
  paths = {};
  std::vector<std::vector<size_t>> n_ids;
  try {
    n_ids = this->solveCP();
  } catch (IloException & e) {
    std::cout << e.getMessage() << std::endl;
  }

  if (n_ids.size() != agents.size()) {
    WARNING(tprintf(
      "[n_steps = @] wrong number of paths @!=@", this->nSteps, n_ids.size(), agents.size()));
    return;
  }
  INFO(tprintf("IDs: \n@", n_ids));

  for (size_t row = 0; row < n_ids.size(); row++) {
    paths.push_back(std::vector<GraphNode>());
    for (size_t v : n_ids[row]) {
      // I really shall assume that the vector is ordered based on the ids, otherwise what's the point of having ids?
      paths.back().push_back(agents[row].getSolver()->getGraph()->getNode(v));
    }
  }
  INFO(tprintf("Paths: \n@", paths));

  if (!paths.empty()) {
    return;
  }
}

std::vector<std::vector<GraphNode>> CPSolver::solve()
{
  // We first need to find the best paths as if the agents are alone in the environment.
  // We are only interested in keeping the size of longest path.
  std::vector<std::vector<GraphNode>> paths = {};

  // Used to avoid computing the initial estimate of the paths
  bool skip = this->nSteps != 0;

  // Compute the shortest path for each agent
  if (!skip) {
    for (Agent & a : this->agents) {
      std::vector<GraphNode> path = a.solve();
      paths.push_back(path);

      this->nSteps = std::max(this->nSteps, path.size());
    }
    INFO(tprintf("Initial paths: \n@", printTablePaths(paths)));
  }

  if (this->steps.empty()){
    this->steps.resize(this->nIterations);
    std::iota(this->steps.begin(), this->steps.end(), this->nSteps);
  }

  // Check if the solution found has conflicts
  if (skip || !findConflict(agents).isNone()) {
    // First check that the problem is even solvable
    if (this->checkComputability) {
      size_t minSteps = this->nSteps;
      this->nSteps += this->nIterations;
      INFO(tprintf("First checking maximum number of steps @", this->nSteps));
      _solve(paths);
      if (paths.empty()) {
        WARNING("The problem is not solvable");
        return {};
      }
      this->nSteps = minSteps;
    }

    // Then solve the constraint programming problem
    for (size_t step : this->steps){
      if (step < 1) { continue; }
      this->nSteps = step;
      _solve(paths);
      if (!paths.empty()){
        break;
      }
    }
  }

  // TODO This may be moved to the postprocessing function if there is a nice way of handling that
  if (paths.size() == this->agents.size()){
    for (size_t a_id = 0; a_id < this->agents.size(); a_id++){
      this->agents[a_id].setPath(paths[a_id]);
    }
  }

  return paths;
}

std::vector<std::vector<size_t>> CPSolver::solveCP()
{
  INFO(tprintf("Solving with @ steps", this->nSteps));
  IloEnv env;

  IloInt max_goals = 0;
  for (const auto & a : this->agents) {
    max_goals = max_goals > a.getGoals().size() ? max_goals : a.getGoals().size();
  }

  IloInt n_agents = this->agents.size();
  IloInt n_nodes = this->agents[0].getSolver()->getGraph()->getNNodes();

  IloIntRange agentsR(env, 0, n_agents);
  IloIntRange nodesR(env, 0, n_nodes);
  IloIntRange stepsR(env, 0, this->nSteps);

  /////////////////////////////////////////////////////
  /////////////////// DECISION VAR ////////////////////
  /////////////////////////////////////////////////////

  IloArray<IloArray<IloBoolVarArray>> x(env, this->nSteps);
  FOREACH(s, stepsR)
  {
    x[s] = IloArray<IloBoolVarArray>(env, n_nodes);
    FOREACH(n, nodesR)
    {
      x[s][n] = IloBoolVarArray(env, n_agents);
      FOREACH(a, agentsR) { x[s][n][a] = IloBoolVar(env); }
    }
  }

  IloArray<IloIntVarArray> goal_points(env, n_agents);
  FOREACH(a, agentsR)
  {
    goal_points[a] = IloIntVarArray(env, max_goals);
    FOREACH(g, IloIntRange(env, 0, max_goals)) { goal_points[a][g] = IloIntVar(env); }
  }

  IloArray<IloArray<IloBoolVarArray>> edges(env, n_nodes);
  FOREACH(n1, nodesR)
  {
    edges[n1] = IloArray<IloBoolVarArray>(env, n_nodes);
    FOREACH(n2, nodesR)
    {
      edges[n1][n2] = IloBoolVarArray(env, this->nSteps);
      FOREACH(s, stepsR) { edges[n1][n2][s] = IloBoolVar(env); }
    }
  }

  IloArray<IloBoolVarArray> movement_cost(env, n_agents);
  FOREACH(a, agentsR)
  {
    movement_cost[a] = IloBoolVarArray(env, this->nSteps);
    FOREACH(s, IloIntRange(env, 0, this->nSteps - 1)) { movement_cost[a][s] = IloBoolVar(env); }
  }

  IloModel model(env, "MAOF");
  //  IloCP cplex(env);
  IloCplex cplex(env);

  /////////////////////////////////////////////////////
  //////////////////// CONSTRAINTS ////////////////////
  /////////////////////////////////////////////////////

  // The agents start at their initial position
  FOREACH(a, agentsR) { model.add(x[0][this->agents[a].getInitPos().getId()][a] == 1); }

  // At each step, there cannot be more than one agent per node
  FOREACH(s, stepsR)
  {
    FOREACH(n, nodesR) { model.add(IloSum(x[s][n]) <= 1); }
  }

  // At each timestamp, an agent is in only one node
  FOREACH(a, agentsR)
  {
    FOREACH(s, stepsR)
    {
      IloExpr expr(env);
      FOREACH(n, nodesR) { expr += x[s][n][a]; }
      model.add(expr <= 1);
    }
  }

  // An agent can move only on the neighbors of the node on which it is
  FOREACH(a, agentsR)
  {
    FOREACH(s, IloIntRange(env, 0, this->nSteps - 1))
    {
      FOREACH(n1, nodesR)
      {
        IloExpr sum(env);
        FOREACH(n2, nodesR) { sum += this->agents[a].getSolver()->getGraph()->getCost(n1,n2,s) * x[s + 1][n2][a]; }
        model.add(IloIfThen(env, x[s][n1][a] == 1, sum >= 1));
      }
    }
  }

  // Keep track of the movement: the movement of the cost is the cost of the link. Moreover, when
  // moving, the edge in that timestamp is set to 1 to avoid swap conflicts.
  FOREACH(a, agentsR)
  {
    FOREACH(s, IloIntRange(env, 0, this->nSteps - 1))
    {
      FOREACH(n1, nodesR)
      {
        FOREACH(n2, nodesR)
        {
          model.add(IloIfThen(
            env, (x[s][n1][a] == 1 && x[s + 1][n2][a] == 1),
            (movement_cost[a][s] == (this->agents[a].getSolver()->getGraph()->getCost(n1,n2,s) * (n1 != n2)) && edges[n1][n2][s] == 1)));
        }
      }
    }
  }

  // Two agents cannot exchange positions
  FOREACH(s, IloIntRange(env, 0, this->nSteps - 1))
  {
    FOREACH(n1, nodesR)
    {
      FOREACH(n2, nodesR)
      {
        if (n1 == n2) {
          continue;
        }
        //        model.add((edges[n1][n2][s] == 1 && edges[n2][n1][s] == 0) || (edges[n1][n2][s] == 0 && edges[n2][n1][s] == 1));
        model.add(IloIfThen(env, edges[n1][n2][s] == 1, edges[n2][n1][s] == 0));
        model.add(IloIfThen(env, edges[n2][n1][s] == 1, edges[n1][n2][s] == 0));
      }
    }
  }

  // At the end the agents are on their final positions
  FOREACH(a, agentsR) { model.add(x[this->nSteps - 1][this->agents[a].getEndPos().getId()][a] == 1); }

  // An agent can reach the final position once all the goals have been taken
  FOREACH(a, agentsR)
  {
    IloExpr sum(env);
    FOREACH(g, IloIntRange(env, 0, this->agents[a].getGoals().size())) { sum += (goal_points[a][g] >= 1); }
    model.add(IloIfThen(env, sum == this->agents[a].getGoals().size(), x[this->nSteps - 1][this->agents[a].getEndPos().getId()][a] == 1));
  }

  // The agents must pass through the goals
  FOREACH(a, agentsR)
  {
    FOREACH(s, IloIntRange(env, 0, this->nSteps))
    {
      FOREACH(g, IloIntRange(env, 0, this->agents[a].getGoals().size()))
      {
        model.add(IloIfThen(env, goal_points[a][g] == s, x[s][this->agents[a].getGoals()[g].getId()][a] == 1));
      }
    }
  }

  // The goals must be taken before the end of the execution and after the start of the execution
  FOREACH(a, agentsR)
  {
    FOREACH(g, IloIntRange(env, 0, this->agents[a].getGoals().size()))
    {
      model.add(goal_points[a][g] <= IloInt(this->nSteps - 2));  // The goal cannot be taken in the last step
      model.add(goal_points[a][g] >= 1);            // The goal cannot be taken in the first step
    }
  }

  // The goals must be taken in the correct order
  FOREACH(a, agentsR)
  {
    FOREACH(g, IloIntRange(env, 1, this->agents[a].getGoals().size()))
    {
      model.add(goal_points[a][g - 1] <= goal_points[a][g] - 1);
    }
  }

  IloExpr MKS(env);
  FOREACH(a, agentsR) { MKS = IloMax(0, IloSum(movement_cost[a])); }

  IloExpr SIC(env);
  FOREACH(a, agentsR) { SIC += IloSum(movement_cost[a]); }

  switch (this->getCostFunction()) {
    case COST_FUNCTION::SIC: {
      model.add(IloMinimize(env, SIC));
      break;
    }
    case COST_FUNCTION::MKS: {
      model.add(IloMinimize(env, MKS));
      break;
    }
    case COST_FUNCTION::NONE:
    default:
      break;
  }

  std::stringstream out;

  cplex.extract(model);
  cplex.setOut(out);
  if (cplex.solve()) {
    INFO(tprintf("Solution found with steps: @", this->nSteps));

    // Save results in vector and return it
    std::vector<std::vector<size_t>> sol;
    FOREACH(a, agentsR)
    {
      sol.push_back(std::vector<size_t>());
      FOREACH(s, stepsR)
      {
        FOREACH(n, nodesR)
        {
          if ((bool)(cplex.getValue(x[s][n][a]))) {
            sol.back().push_back(n);
            break;
          }
        }
      }
    }
    try {
      DISPLAY_RESULTS();
    } catch (const std::exception& e) {
      WARNING(e.what());
    }

    return sol;

  } else {
    WARNING(tprintf("No solution found with steps: @", this->nSteps));
    WARNING(out.str());

    return {};
  }
}
