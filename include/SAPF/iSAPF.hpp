/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @file iSAPF.hpp
 * @brief Interface to import and select the correct SAPF solver.
 */

#ifndef MAOF_ISAPF_HPP
#define MAOF_ISAPF_HPP

#include <SAPF/AStar/AStar.hpp>
#include <SAPF/ST/ST.hpp>
#include <SAPF/TDSP/TDSP.hpp>
#include <Utils.hpp>

/**
 * @brief Function to take a string in input and return a pointer to the corresponding SAPF solver
 * @details The arguments that should not be used are not passed to the solver. For example,
 * TDSPSolver does not use any heuristic, hence it will not be passed. The string is first converted
 * to upper case
 * @param sapfSolver The string with the SAPF solver to use
 * @param graph The graph on which the agents should move
 * @param heurFunc The heuristic function to be used. Default is nullptr
 * @return A pointer to the corresponding `SAPFSolver`
 */
inline SAPFSolver * chooseSAPFSolver (std::string sapfSolver, std::shared_ptr<Graph> & graph, heurFunc heurFunc = nullptr)
{
  s_toupper(sapfSolver);
  if (sapfSolver == "ST"){
    WARNING("This SAPFSolver is being deprecated.")
    return new STSolver(graph);
  }
  else if (sapfSolver == "TDSP"){
    return new TDSPSolver(graph);
  }
  else if (sapfSolver == "ASTAR" || sapfSolver == "A*"){
    return new AStarSolver(graph, heurFunc);
  }
  else {
    ERROR("No such SAPF solver \"@\"", sapfSolver);
  }
}

#endif  //MAOF_ISAPF_HPP
