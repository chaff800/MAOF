/**
* @author Enrico Saccon <enrico.saccon@unitn.it>
* @file ICR.hpp
* @brief Increase Conflict Resolution (ICR) finds the SAPF solutions for each agent, checks if there
* are conflicts, and for each conflict it extracts a sub MAPF problem considering only a portion of
* the map and the involved agents, solving it with a MAPF solver.
* @details See README.md for a general idea of how the algorithm works and the documentation of the
* functions for the `ICRSolver` class for a more detailed description.
*/

#ifndef MAOF_ICR_HPP
#define MAOF_ICR_HPP

class ICRSolver;  // Forward declaration

// System includes
#include <vector>
#include <queue>
#include <map>

// Library includes
#include <MAPF/iMAPF.hpp>
#include <Utils.hpp>
#include <MAPF/Constraint.hpp>  // TODO separate adding constraints from adding placeholders
#include <ConMatrix.hpp>        // TODO Add function to return type of graph and also create graph

static const size_t MAX_HOPS = -1;  ///< The maximum hops that should be checked before considering the whole map

/**
 * @brief Finds the new node given the old id
 * @param map The map of the nodes
 * @param id The old id
 * @return An iterator to the new node
 */
inline auto findNewNode(const std::map<int,int>& map, size_t id) { return map.find(-1 * (id+1)); }
/**
 * @brief Gets the new id given the old id
 * @param map The map of the nodes
 * @param id The old id
 * @return The new id
 */
inline auto getNewId(const std::map<int,int>& map, size_t id) { return map.at(-1 * (id+1)); }
/**
 * @brief Finds the old node given the new id
 * @param map The map of the nodes
 * @param id The new id
 * @return An iterator to the old node
 */
inline auto findOldNode(const std::map<int,int>& map, size_t id) { return map.find(id+1); }
/**
 * @brief Gets the old id given the new id
 * @param map The map of the nodes
 * @param id The new id
 * @return The old id
 */
inline auto getOldId(const std::map<int,int>& map, size_t id) { return map.at(id+1); }

class ICRSolver : public MAPFSolver
{
private:
  MAPF_TYPE subSolverType;  ///< The sub-solver to be used to solve the local conflicts
  COST_FUNCTION cf;         ///< The cost function to be minimized
  size_t n_hops;            ///< The number of intersection to consider to solve the problem

public:
  /**
   * Default constructor
   */
  ICRSolver() : MAPFSolver(), subSolverType(MAPF_TYPE::NONE), cf(COST_FUNCTION::NONE), n_hops(0) {}

  /**
   * @brief Constructor that sets all the values for the `ICRSolver`
   * @details If _n_hops is not passed, then it is computed by first exploring the map and setting
   * it to the number of nodes with 3 or more neighbors
   * @param agents The agents that are part of the MAPF problem
   * @param _subSolverType The `MAPF_TYPE` of the sub-solver to be used
   * @param _cf The cost function as `COST_FUNCTION` to be minimized
   * @param _n_hops The number of intersections, i.e., the number of nodes with 3 or more neighbors
   * that should be checked before solving the whole problem with the sub-solver.
   */
  ICRSolver(
    std::vector<Agent> agents, MAPF_TYPE _subSolverType, COST_FUNCTION _cf,
    size_t _n_hops = MAX_HOPS)
  : MAPFSolver(agents), subSolverType(_subSolverType), cf(_cf)
  {
    if (_n_hops != MAX_HOPS) {
      this->n_hops = _n_hops;
    } else {
      this->n_hops = this->agents[0].getSolver()->getGraph()->getNodesWithDegree(3) + 1;
    }
  }

  ~ICRSolver() override {}

  /**
  * @brief Function that solves the MAPF problem. Check README.md for a brief introduction.
  * @details The algorithm first solves the SAPF problems. Then it calls the function `findConflict`
  * to check if there are conflicts between the agents. If there are not, then the algorithm returns
  * the SAPF paths. If instead there are conflicts, then it loops until all the conflicts are
  * resolved. To do this is enters a loop over the possible number of intersection to considers. It
  * starts by considering only one intersection, and increasing it by one at each iteration. Every
  * time, it calls the `extractGraph()` function to extract a sub-graph with the specified number of
  * intersection around the node of conflict. If the map returned is empty, then the algorithm uses
  * the sub-solver over the whole map since it means that it is not possible to sort the conflict
  * out in a sub-graph. If instead the map is not empty, then the `solveLocal()` function is called.
  *
  * This function will return a pair containing the new path for the agents in the conflict (even
  * those that were indirectly added, because they were part of the sub-graph) and containing the
  * ids of the agents for which the paths should be replaced. If `solveLocal()` returns a solution,
  * then the algorithm replaces the correct paths and breaks the local loop, i.e., the one
  * increasing `hop` and checks again for conflicts. Instead, if `solveLocal()` was not able to find
  * a solution, then the previous paths are restored inside the agent (since the `SAPFSolver` for
  * each agent is a pointer, when calling it in the sub-solver, the paths may be changed causing
  * problems).
  * @return A `std::vector<std::vector<GraphNode>>` containing the paths for each agent.
  */
  std::vector<std::vector<GraphNode>> solve() override;

  /**
  * @brief Function used to solve the ICR sub-problem. It simply throws a runtime error since ICR
  * cannot be used to solve an ICR sub-problem.
  * @throws std::runtime_error
  * @param times The deferred times at which each agent starts.
  * @return Nothing.
  */
  std::vector<std::vector<GraphNode>> solveICR(const std::vector<size_t> & times) override
  {
    throw std::runtime_error("Cannot solve ICR with ICR");
  }

  /**
  * Function to set the cost function to be used.
  * @param _cf The cost function as `COST_FUNCTION` to be used.
  */
  void setCostFunction(COST_FUNCTION _cf) { this->cf = _cf; }
  /**
  * Function to get the cost function.
  * @return The cost function as `COST_FUNCTION`.
  */
  COST_FUNCTION getCostFunction() const { return this->cf; }

  /**
  * Function to set the sub-solver to be used.
  * @param _cf The sub-solver as `MAPF_TYPE` to be used.
  */
  void setSubSolver(MAPF_TYPE _subSolver) { this->subSolverType = _subSolver; }
  /**
  * Function to get the used sub-solver.
  * @return The sub-solver as `MAPF_TYPE`.
  */
  MAPF_TYPE getSubSolver() const { return this->subSolverType; }

  /**
  * Dummy function to override the `MAPFSolver` virtual function.
  * @param n
  */
  void postProcess(std::vector<std::vector<GraphNode>> & n) override
  {
    std::cout << "postProcess " << n.size() << std::endl;
  }

  /**
  * @brief This function extracts a new graph from the original one, considering the number of
  * intersections (hops).
  * @details For both agents of the conflicts, the algorithm considers the node on which they were
  * at time \f$t\f$ (the time of conflict), it adds said node to the new-graph and then for each
  * neighbor of the node, it iteratively add the neighbors until the number of intersections in the
  * new graph is greater or equal than the value `hops`. Once that is done, it creates a map linking
  * new nodes with old nodes and vice versa. For example, a node with id 5 on the whole map is
  * inserted inside the new graph with id 1. Then, to keep track of this change, the algorithm adds
  * two entry to the map: `map[newId+1] = 5` and `map[-1*(oldId+1)] = 1`. This will allow us to know
  * which new node corresponds to which old node and vice versa. Notice that the auxiliary functions
  * (defined in ICR.cpp) `findNewNode`, `findOldNode`, `getNewId` and `getOldId` can be used to
  * easily retrieve these pieces of information.
   *
  * Then it creates the new graph with the new nodes keeping the old connections. The type of the
  * graph is the same as the type of the whole graph.
   *
  * Notice that when the whole graph is considered as the graph of the sub-problem, then the second
  * value returned is an empty map, which is checked inside the `solve()` function.
  * @param a1 The first agent of the conflict.
  * @param a2 The second agent of the conflict.
  * @param t The time at which the conflict happened.
  * @param hops The number of intersections to be considered in the new graph.
  * @return A `std::pair` containing a `std::shared_ptr` to the new graph and a `std::map` stating
  * which node on the new map corresponds to which node on the old map.
  */
  std::pair<std::shared_ptr<Graph>, std::map<int, int>> extractGraph(
    Agent & a1, Agent & a2, size_t t, size_t hops = 1);

  /**
  * @brief This function takes a sub-graph, selects the agents inside said sub-graph and solves
  * the local problem.
  * @details The algorithm first calls the `populateNewAgents()` function which checks which of the
  * agents pass through the graph at the time of the conflict and returns a vector of new agents.
  * Mind that, while these agent are copies of the old agents, the pointer to the SAPF solver is the
  * same as the old ones, hence computing a new solution for the new agent changes the values of the
  * old solution too. Moreover it also returns a vector of pairs stating the time of entry and exit
  * from the sub-graph of each agent.
   *
  * The algorithm then loops through pairs of agents to check if they have the same initial or final
  * positions as that is not considered valid. If they do, the function returns an empty solution.
  * If instead the scenario is valid, then it stores all the times at which the agents enters the
  * sub-graph and calls the sub-solver with the corresponding `solveICR()` function passing said
  * timestamps. These are going to be used if the agents have deferred times of entering the graph
  * since it's an important information for the sub-solver in order to correctly avoid conflicts.
  * If the sub-solver does not return any local path, then no solution was available and the
  * function returns an empty pair. If the sub-solver was able to find a local solution, then we
  * express the nodes in the solution as the old version of the node by simply changing the id of
  * the node and then merge the paths. For each agent which was in the conflict, the path before
  * entering the sub-graph is maintained since it's fine, as is the path after exiting the sub-graph
  * (if any). The part inside the subgraph is instead changed and this is done by simply inserting
  * the new local path at the timestamp in which the agent entered the sub-graph. Notice that since
  * we have passed the timestamps to the `solveICR()` function, the local paths will be longer and
  * the part in which we are interested in is actually the part from the timestamp-th node to the
  * end. Once this is done, we can return the pair of new paths and the vector of agent ids that
  * have been part of the sub-problem.
  * @param newGraph A reference to a `std::shared_ptr` to the new sub-graph.
  * @param nodeNodeMap A reference to the `std::map` mapping each new node and old node together.
  * @param cons A vector of constraints since in each conflict there are at least two constraints.
  * @return A pair containing the paths to replace for the agents inside the sub-problem and the ids
  * of said agents.
  */
  std::pair<std::vector<std::vector<GraphNode>>, std::vector<size_t>> solveLocal(
    std::shared_ptr<Graph> & newGraph, std::map<int, int> & nodeNodeMap,
    const std::vector<Constraint> cons);

  /**
   * @brief This function takes a sub-graph, selects the agents inside said sub-graph changing their
   * initial and final position, and choosing the correct goal nodes inside the sub-graph.
   * @details The algorithm loops over all the agents. For each agent it computes the possible time
   * of conflict which is either:
   * - the time of conflict if the agent is in the conflict;
   * - the maximum time at which the (different) agents had the conflict, if the path of the
   * considered agent is long enough, OR
   * - the maximum length of the path - 1, if the path of the considered agent is not long enough.
   *
   * Then it checks if the agent is inside the sub-graph at the time of conflict. If it is not, then
   * it continues to the next agent. If it is, then it computes the initial and final nodes in which
   * the agent enters and exits the sub-graph and the times in which it does so. It then computes
   * all the goals that the agent reaches inside the su-graph. If the goal is the initial position
   * in the sub-graph, or the final position in the sub-graph, then they are not considered for the
   * sub-problem since they will be met for sure.
   *
   * Once all the agents have been considered, the algorithm returns a vector of new agents.
   * @param newGraph The new graph for the sub-problem.
   * @param nodeNodeMap The `std::map` mapping each new node and old node.
   * @param conTimestamps A `std::map` mapping the agents in the conflict, through their id (key) to
   * the timestamp of the conflict (value).
   * @param timetamps A vector of timestamps that will be filled with `std::pair`s with the times at
   * which the agent enters and exits the sub-graph.
   * @param agentAgentMap A `std::map` mapping each new agent and old agent.
   * @return A vector containing the new agents for the sub-problem.
   */
  std::vector<Agent> populateNewAgents(
    std::shared_ptr<Graph> & newGraph, const std::map<int, int> & nodeNodeMap,
    const std::map<size_t, size_t> & conTimestamps,
    std::vector<std::pair<size_t, size_t>> & timetamps, std::map<size_t, size_t> & agentAgentMap);

  /**
  * @brief Function that stores info about the `ICRSolver` in a string.
  * @return A `std::string` containing information regarding the `ICRSolver`.
  */
  [[nodiscard]] std::string out() const override
  {
    std::stringstream stream;
    stream << "Using ICR" << std::endl;
    stream << "Subsolver: " << mapfName(this->subSolverType) << std::endl;
    stream << "Number of agents: " << this->getAgents().size() << std::endl;
    stream << "Number of nodes: " << this->getAgents()[0].getSolver()->getGraph()->getNNodes()
           << std::endl;
    stream << "Maximum number of hops: " << this->n_hops << std::endl;
    stream << tprintf("Nodes: @\n", this->getAgents()[0].getSolver()->getGraph()->getNodes());

    stream << "Agents:" << std::endl;
    for (const Agent & a : this->getAgents()) {
      stream << tprintf("- @ @->@ goals: @\n", a, a.getInitPos(), a.getEndPos(), a.getGoals());
    }
    return stream.str();
  }

  /**
  * @brief Function that prints the `ICRSolver` to a stream.
  * @param stream The stream to which the `ICRSolver` is printed.
  * @param sol The `ICRSolver` to be printed.
  * @return The stream to which the `ICRSolver` was printed.
  */
  friend std::ostream & operator<<(std::ostream & stream, const ICRSolver & sol)
  {
    stream << sol.out();
    return stream;
  }
};

#endif  //MAOF_ICR_HPP
