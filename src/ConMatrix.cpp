/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file ConMatrix.cpp
 * @brief 
 */

#include <ConMatrix.hpp>

// TODO Missing tests for this function
/*!
 * @brief Function that adds some virtual nodes where the agent can wait to avoid conflicts.
 * @details The function consider two possibilities of conflicts: vertex and edge conflicts. A vertex conflict is in the
 * form \f$(n, t)\f$ where \f$n1\f$ is the node where the agent should not be and \f$t\f$ the time at which it cannot be
 * on the node. An edge conflict is in the form \f$(n1, n2, t)\f$ indicating that the agent cannot use the connection
 * from \f$n1\f$ to \f$n2\f$ at time \f$t\f$.
 * Vertex conflicts are dealt with by adding a placeholder which inherits all the connection directed to \f$n1\f$ and
 * adds a connection towards the original node. Moreover, the connections towards \f$n1\f$ are limited so that they can
 * be used to reach \f$n1\f$ only when the time is different from \f$t\f$. The connection from the placeholder to
 * \f$n1\f$ is valid only for \f$t+1\f$ since it models the idea: "wait on the previous node for a timestamp".
 * Edge conflicts instead are more specific since only the interested connection is limited for only the timestamp. All
 * other connections from \f$n1\f$ are valid and can be used. The placeholder does not inherit any other connection
 * towards it, but only \f$n1\f$, plus the connection from the placeholder to \f$n2\f$ which is valid only at time
 * \f$t+1\f$.
 * @param nodes the list of nodes as a reference since the placeholders will be added to this list
 * @param connect the initial connectivity matrix
 * @param constraints a list of CBSConstraints
 * @param placeholders a pointer to a vector which will be filled with nodes, indicating that that
 * node is placeholder.
 * @return a new connectivity matrix made of Connection objects.
 */

void ConMatrix::addConstraints(std::vector<Constraint> constraints, std::vector<GraphNode>* placeholders)
{
  std::sort(
    constraints.begin(), constraints.end(), [](const Constraint & a, const Constraint & b) {
      if (a.getN1().getV() < b.getN1().getV()) {
        return true;
      } else if (a.getN1().getV() == b.getN1().getV() && a.getTimestamp() < b.getTimestamp()) {
        return true;
      } else {
        return false;
      }
    });

  for (Constraint con : constraints) {
    size_t t = con.getTimestamp();
    // Add new placeholder
    // The new node id will start from the highest id to avoid conflicts.

    if (placeholders != nullptr) {
      GraphNode placeholder = GraphNode(nodes.size() + 1);
      nodes.push_back(placeholder);
      placeholders->push_back(placeholder);

      // The connections to the placeholder are the same as the old node
      // Since the placeholder is unique for a constraint, then it will always be LIMIT_ALWAYS and it
      // will always store only one timestamp
      for (size_t i = 0; i < this->matrix.size(); i++) {
        this->matrix[i].resize(this->matrix[i].size() + 1);
        if (this->matrix[i][con.getN1().getId()].cost(t)) {
          this->matrix[i].back() =
            Connection(Connection::TYPE::LIMIT_ALWAYS, 1, {std::make_pair(t, con.getAId())});
        } else {
          this->matrix[i].back() = Connection(Connection::TYPE::ZERO);
        }
      }

      // Add new row for the connection going from the placeholder to N1, while all the other
      // connections are ZERO.
      this->matrix.push_back(std::vector<Connection>(this->matrix.back().size()));
      for (size_t i = 0; i < this->matrix.back().size(); i++) {
        this->matrix.back()[i] =
          i != con.getN1().getId()
            ? Connection(Connection::TYPE::ZERO)
            : Connection(Connection::TYPE::LIMIT_ALWAYS, 1, {std::make_pair(t + 1, con.getAId())});  //Since I want to leave after the constraint
      }
    }

    //All the connections to N1 must be limited so that they are valid only when the constraints are
    // not enforced.
    for (size_t i = 0; i < this->matrix.size() - 1; i++) {
      switch (this->matrix[i][con.getN1().getId()].getType()) {
        case Connection::TYPE::ONE: {
          this->matrix[i][con.getN1().getId()].setType(Connection::TYPE::LIMIT_ONCE);
          this->matrix[i][con.getN1().getId()].addTime(t, con.getAId());
          break;
        }
        case Connection::TYPE::LIMIT_ONCE: {
          this->matrix[i][con.getN1().getId()].addTime(t, con.getAId());
          break;
        }
        case Connection::TYPE::LIMIT_ALWAYS: {
          if (this->matrix[i][con.getN1().getId()].cost(t)) {
            this->matrix[i][con.getN1().getId()].removeTime(t);
          }
          break;
        }
        case Connection::TYPE::ZERO:
        default:
          break;
      }
    }
  }
}