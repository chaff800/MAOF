/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @file GraphTest.cpp
 */

#ifdef GTEST
//Library includes
#include <ConMatrix.hpp>
#include <Utils.hpp>

// System includes
#include <gtest/gtest.h>
#include <stdlib.h>
#include <iostream>

#define CON_ONE Connection(Connection::TYPE::ONE)
#define CON_ZERO Connection(Connection::TYPE::ZERO)
#define CON_ONCE(x) Connection(Connection::TYPE::LIMIT_ONCE, 1, (std::vector<TM_ID>)x)
#define CON_ALWAYS(x) Connection(Connection::TYPE::LIMIT_ALWAYS, 1, (std::vector<TM_ID>)x)

#define L_ONCE Connection::TYPE::LIMIT_ONCE

TEST(ConMatrix, ConMatrixPrint)
{
  std::vector<std::vector<int>> connect = {
    {1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1}, {0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1},
    {1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1}, {0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1},
    {0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1}, {1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1},
    {0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1}, {0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1}, {0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1},
    {1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1}, {0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1}
  };

  std::vector<GraphNode> nodes = {};
  for(size_t i=0; i<connect.size(); i++){
    nodes.push_back(GraphNode(i+1));
  }

  ConMatrix con(nodes, connect);

//  std::cout << con << std::endl;
}

/**
 * Tests to verify that the `Connection` object can be directly accessed and changed.
 */
TEST(ConMatrix, ConMatrixConnectionAccess)
{
  std::vector<std::vector<int>> connect = {
    {1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1}, {0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1},
    {1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1}, {0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1},
    {0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1}, {1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1},
    {0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1}, {0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1}, {0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1},
    {1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1}, {0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1}
  };

  std::vector<GraphNode> nodes = {};
  for(size_t i=0; i<connect.size(); i++){
    nodes.push_back(GraphNode(i+1));
  }

  ConMatrix con(nodes, connect);

  EXPECT_TRUE(con.getCost(0, 1) == 0);
  con.getConnection(0, 1).setType(Connection::TYPE::ONE);
  EXPECT_TRUE(con.getCost(0, 1) == 1);
}

// TODO add tests for getters and setters

#endif // GTEST