#ifndef MAOF_PRIORITY_QUEUE_HPP
#define MAOF_PRIORITY_QUEUE_HPP

/**
 * @author Diego Planchenstainer and Giovanni Lorenzini
 */

#include <queue>
#include <limits>
#include <utility>

using std::size_t;

template <typename T, typename priority_t>
struct PriorityQueue
{
  typedef std::tuple<priority_t, size_t, T> PQElement;
  std::priority_queue<PQElement, std::vector<PQElement>, std::greater<PQElement>> elements;
  size_t counter = SIZE_MAX;

  inline bool empty() const { return elements.empty(); }

  inline size_t size() const { return SIZE_MAX - counter; }

  inline void put(T item, priority_t priority) { elements.emplace(priority, counter--, item); }

  T get()
  {
    T best_item = std::get<2>(elements.top());
    elements.pop();
    return best_item;
  }
};

#endif  //MAOF_PRIORITY_QUEUE_HPP
