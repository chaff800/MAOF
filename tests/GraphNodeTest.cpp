//
// Created by Enrico Saccon on 26/07/22.
//
#ifdef GTEST
#include <gtest/gtest.h>

#include <GraphNode.hpp>

TEST(GraphNode, GraphNodeConstructorVoid)
{
  GraphNode node = GraphNode();
  EXPECT_EQ(0, node.getId());
  EXPECT_EQ(0, node.getV());
}

TEST(GraphNode, GraphNodeConstructor1)
{
  GraphNode node(1, 2);
  EXPECT_EQ(1, node.getId());
  EXPECT_EQ(2, node.getV());
}

TEST(GraphNode, GraphNodeConstructor2)
{
  GraphNode node(-1, 2);
  EXPECT_EQ(-1, node.getId());
  EXPECT_EQ(2, node.getV());
}

TEST(GraphNode, GraphNodeSet)
{
  GraphNode node = GraphNode();
  node.setId(3);
  node.setV(5);

  EXPECT_EQ(3, node.getId());
  EXPECT_EQ(5, node.getV());
}

#endif