#!/usr/bin/env/ python3

import json
import networkx as nx
import matplotlib.pyplot as plt
from Node import Node
xMin = 17
xMax = 20
yMin = 13
yMax = 22
filename = "output/wh2_2_1/wh2_2_1"

G = nx.DiGraph()
# create nodes
nNodes = 0
nodes = []
labeldict = {}
for i in range(xMin,xMax+1):
    for j in range(yMin,yMax+1):
        string = "{:d}{:02d}".format(i, j)
        nodes.append(Node(int(string), nNodes, i, j))
        print(nodes[-1])
        G.add_node(nNodes, pos = (j, i))
        labeldict[nNodes] = string
        nNodes += 1

print(nNodes)

# create connections
dY = yMax - yMin + 1
connect = [[0 for x in range(nNodes)] for x in range(nNodes)]
for i in range(xMin,xMax+1):
    idx = i-xMin
    for j in range(yMin,yMax+1):
        jdx = j-yMin
        nid = idx*dY + jdx
        print(nid, idx, jdx, i, j)
        connect[nid][nid] = 1
        G.add_weighted_edges_from([(nid, nid, 1)])
        if jdx>0: 
            connect[nid][nid-1] = 1
            G.add_weighted_edges_from([(nid, nid-1, 1)])
        if jdx<dY - 1:
            connect[nid][nid+1] = 1
            G.add_weighted_edges_from([(nid, nid+1, 1)])
        if i<xMax and j == yMax:
            nextId = (idx+1)*dY + dY - 1
            connect[nextId][nid] = 1
            connect[nid][nextId] = 1
            G.add_weighted_edges_from([(nextId, nid, 1)])
            G.add_weighted_edges_from([(nid, nextId, 1)])
        if i<xMax and j == yMin:
            nextId = (idx+1)*dY
            connect[nextId][nid] = 1
            connect[nid][nextId] = 1
            G.add_weighted_edges_from([(nextId, nid, 1)])
            G.add_weighted_edges_from([(nid, nextId, 1)])
        
for i in range(0, xMax-xMin+1):
    nid = i*dY + (21 - yMin)
    connect[nid][nid+1] = 10
    connect[nid+1][nid] = 10
    print(i, 21-yMin, connect[i][21-yMin], nid)
    G[nid][nid+1]["weight"] = 10
    G[nid+1][nid]["weight"] = 10

counter = 0
for line in connect:
    counter += 1 
    print(counter, line)

labels = nx.get_edge_attributes(G,'weight')
nx.draw_networkx_edge_labels(G,nx.get_node_attributes(G, 'pos'),edge_labels=labels)
nx.draw(G, nx.get_node_attributes(G, 'pos'), labels = labeldict, with_labels=True, node_size = 100, font_size = 3)
plt.gca().invert_yaxis()
# plt.show()
plt.savefig(filename+".pdf", dpi = 1200, orientation = "landscape", transparent = False)
plt.savefig(filename+".png", dpi = 1200, orientation = "landscape", transparent = False)

with open("temp.json", "w+") as output:
    json.dump({
        "nNodes" : nNodes,
        "nodes" : [n.toList() for n in nodes],
        "connect" : connect 
    }, output, indent = 2)

with open("temp.json", "r") as temp: 
    with open(filename+".json", "w+") as output:
        for line in temp:
            if any(x in line for x in ["{" , "}", "nNodes", "nodes", "connect", "]"]):
                output.write(line)
            else:
                line = line.split("\n")[0]
                output.write(line)

