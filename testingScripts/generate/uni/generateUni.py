import json
import os
import random
import sys

import networkx as nx
import matplotlib.pyplot as plt


agents = [2,4,8]
goals = [0,1,5,10]
MAPF = "CBS"
SAPF = "TDSP"
costFunction = "SIC"
heuristic = ""


def drawGraph(G):
    labeldict = {}
    for node in G.nodes:
        labeldict[node] = G.nodes[node]["name"]

    plt.figure(figsize=(4096/90, 2160/90))
    labels = nx.get_edge_attributes(G,'weight')
    nx.draw_networkx_edge_labels(G, nx.get_node_attributes(G, 'pos'), edge_labels=labels)
    nx.draw(G, nx.get_node_attributes(G, 'pos'), labels = labeldict, with_labels=True, node_size = 100, font_size = 3)
    plt.show()


def saveGraphImage(filename, G):
    labeldict = {}
    for node in G.nodes:
        labeldict[node] = G.nodes[node]["name"]

    labels = nx.get_edge_attributes(G,'weight')
    plt.figure(figsize=(4096/90, 2160/90))
    nx.draw_networkx_edge_labels(G,nx.get_node_attributes(G, 'pos'),edge_labels=labels)
    nx.draw(G, nx.get_node_attributes(G, 'pos'), labels = labeldict, with_labels=True, node_size = 10000, font_size = 20)
    plt.savefig(filename+".pdf", dpi = 1200, orientation = "landscape", transparent = False)
    # plt.savefig(filename+".png", dpi = 1200, orientation = "landscape", transparent = False)


def getGraphFromJson(filename = "uni_map.json"):
    G = nx.DiGraph()
    with open (filename, "r") as fileJson:
        data = json.load(fileJson)
        for v in data["nodes"]:
            G.add_node(v[1], pos=(v[2], v[3]), name=v[0])

        for row in range(len(data["connect"])):
            for col in range(len(data["connect"][row])):
                if data["connect"][row][col] == 1:
                    G.add_edge(row, col, weight=data["connect"][row][col])

    # Add links to make it connected
    G.add_edge(135, 144, weight=1)
    G.add_edge(143, 145, weight=1)
    G.add_edge(161, 162, weight=1)
    G.add_edge(173, 174, weight=1)
    G.add_edge(185, 186, weight=1)
    G.add_edge(144, 135, weight=1)
    G.add_edge(145, 143, weight=1)
    G.add_edge(162, 161, weight=1)
    G.add_edge(174, 173, weight=1)
    G.add_edge(186, 185, weight=1)

    drawGraph(G)
    return G


def extractBuilding1(G : nx.DiGraph):
    if not os.path.isdir("maps/build1"):
        os.mkdir("maps/build1")

    build = nx.DiGraph()
    nodeMap = {}
    for node in G.nodes:
        if G.nodes[node]["pos"][0] < 74:
            build.add_node(len(build.nodes), pos=G.nodes[node]["pos"], name=G.nodes[node]["name"])
            nodeMap[node] = len(build.nodes) - 1

    for edge in G.edges:
        if edge[0] in nodeMap and edge[1] in nodeMap:
            if nodeMap[edge[0]] in build.nodes and nodeMap[edge[1]] in build.nodes:
                build.add_edge(nodeMap[edge[0]], nodeMap[edge[1]], weight=G.edges[edge]["weight"])

    saveGraphImage("maps/build1/build1", build)
    if nx.is_strongly_connected(build):
        print("Build1 connected")
    else:
        print("Build 1 not connected")

    return build


def extractBuilding2(G : nx.DiGraph):
    if not os.path.isdir("maps/build2"):
        os.mkdir("maps/build2")
    build = nx.DiGraph()
    nodeMap = {}
    for node in G.nodes:
        if G.nodes[node]["pos"][0] > 109:
            build.add_node(len(build.nodes), pos=G.nodes[node]["pos"], name=G.nodes[node]["name"])
            nodeMap[node] = len(build.nodes) - 1

    for edge in G.edges:
        if edge[0] in nodeMap and edge[1] in nodeMap:
            if nodeMap[edge[0]] in build.nodes and nodeMap[edge[1]] in build.nodes:
                build.add_edge(nodeMap[edge[0]], nodeMap[edge[1]], weight=G.edges[edge]["weight"])

    saveGraphImage("maps/build2/build2", build)
    if nx.is_strongly_connected(build):
        print("Build 2 connected")
    else:
        print("Build 2 not connected")

    return build



def extractBridge(G : nx.DiGraph):
    if not os.path.isdir("maps/bridge"):
        os.mkdir("maps/bridge")
    build = nx.DiGraph()
    nodeMap = {}
    for node in G.nodes:
        if G.nodes[node]["pos"][0] < 109 and G.nodes[node]["pos"][0] > 73:
            build.add_node(len(build.nodes), pos=G.nodes[node]["pos"], name=G.nodes[node]["name"])
            nodeMap[node] = len(build.nodes) - 1

    for edge in G.edges:
        if edge[0] in nodeMap and edge[1] in nodeMap:
            if nodeMap[edge[0]] in build.nodes and nodeMap[edge[1]] in build.nodes:
                build.add_edge(nodeMap[edge[0]], nodeMap[edge[1]], weight=G.edges[edge]["weight"])

    saveGraphImage("maps/bridge/bridge", build)
    if nx.is_strongly_connected(build):
        print("Bridge connected")
    else:
        print("Bridge not connected")

    return build


def createRandom(G : nx.DiGraph, outputDir : str):
    if not os.path.isdir(outputDir):
        os.mkdir(outputDir)

    data = dict()
    data["MAPF"] = MAPF
    data["SAPF"] = SAPF
    data["costFunction"] = costFunction
    data["heuristic"] = heuristic
    data["nNodes"] = len(G.nodes)
    data["nodes"] = []
    for node in G.nodes:
        data["nodes"].append((G.nodes[node]["name"], G.nodes[node]["pos"][0], G.nodes[node]["pos"][1], node))
    data["connect"] = nx.adjacency_matrix(G, nodelist=sorted(G.nodes)).todense().tolist()

    counter = 0

    for nAgents in agents:
        if nAgents >= len(data["nodes"]):
            print("Skipping nAgents = " + str(nAgents) + " because it is too big")
            continue
        data["nAgents"] = nAgents
        data["agents"] = [{} for a in range(nAgents)]
        initPos = random.sample(list(data["nodes"]), nAgents)
        endPos = []
        if nAgents > 50 * len(data["nodes"]) / 100.0:
            endPos = initPos
        else:
            endPos = random.sample([n for n in data["nodes"] if n not in initPos], nAgents)

        for a in range(nAgents):
            data["agents"][a]["ID"] = a
            data["agents"][a]["initPos"] = initPos[a]
            data["agents"][a]["endPos"] = endPos[a]

        for nGoals in goals:
            nonEndPos = [n for n in data["nodes"] if n not in endPos]
            for a in range(nAgents):
                data["agents"][a]["goalPos"] = []
                if nGoals > 0:
                    data["agents"][a]["goalPos"].append(random.choice(nonEndPos))
                for i in range(1, nGoals):
                    tmp = random.choice(nonEndPos)
                    while tmp == data["agents"][a]["goalPos"][-1]:
                        tmp = random.choice(nonEndPos)
                    data["agents"][a]["goalPos"].append(tmp)

            # Make sure that no goalPos is also an endPos
            for a in range(nAgents):
                for n in data["agents"][a]["goalPos"]:
                    if n in endPos:
                        raise Exception("goalPos is already an endPos")

            with open("temp.json", "w+") as output:
                json.dump(data, output, indent = 2)

            with open("temp.json", "r") as temp:
                with open(os.path.join(outputDir, str(counter)+".json"), "w+") as output:
                    for line in temp:
                        if any(x in line for x in ["{" , "}", "nNodes", "nodes", "connect", "]", "MAPF", "SAPF", "costFunction", "heuristic", "nAgents", "ID", "goalPos"]):
                            output.write(line)
                        else:
                            line = line.split("\n")[0]
                            output.write(line)

            os.remove("temp.json")

            counter += 1
            print(counter, end="\r")


if __name__ == "__main__":
    random.seed(1)  # for repeatability
    if not os.path.isdir("maps"):
        os.mkdir("maps")
    if not os.path.isdir("maps/uni"):
        os.mkdir("maps/uni")

    G = getGraphFromJson()
    createRandom(G, "maps/uni/output")
    build1 = extractBuilding1(G)
    createRandom(build1, "maps/build1/output")
    bridge = extractBridge(G)
    createRandom(bridge, "maps/bridge/output")
    build2 = extractBuilding2(G)
    createRandom(build2, "maps/build2/output")
