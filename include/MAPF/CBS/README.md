# CBS

Implementation of the Constraint Based Search algorithm proposed by Sharon et al. [1].

## Briefly

The algorithm solves the MAPF problem by managing a Constraint Tree (CT), whose nodes contain:

- The cost of the node as defined by the cost function;
- The paths of the agents;
- A list of constraint.

The algorithm is divided into a high-level search that manages the CT and a low-level search that instead computes the 
path for an agent given the constraints from the list of the considered CT node.

For the low-level search, any algorithm that solves the SAPF problem can be used, given that it has been modified to 
account for constraints. 

The high-level search instead, considers the nodes of the CT that have not yet been explored and, starting from the one
with the lowest cost, checks if in any two paths, two agents $a_i$ and $a_j$ are conflicting. If they do, then two new
CT nodes $N_h, N_l$ are created, each inheriting the list of constraint from the parent and adding for each new node a 
new constraint stating that one of the two agents cannot be on the conflicting graph node at the time of conflict. [^1]

The search continues until a solution without conflicts is found. 

## Implementation

The implementation corresponds strictly to the one described in [1]. 

Please note that at the moment, the only SAPF solver which can be used as for the low-level search is A*.

## Usage

The advice is to use wither the `readJSON` function or the `runTest` one available in `Test.hpp`. The first will return
a pointer to a `MAPFSolver` (which can then be dynamically cast to a `CBS`) while the second will run the whole solver
on its own.

If you want to use the solver in a different way, you can create the solver as follows:

```cpp
#include <MAPF/CBS/CBS.hpp>
CBSSolver solver (agents, cf);
```

where `agents` is a vector of `Agent` and `cf` is a `COST_FUNCTION` value. Then, you can use the `solve` function.

## References

[1] Sharon, G., Stern, R., Felner, A., & Sturtevant, N. R. (2015). Conflict-based search for optimal multi-agent pathfinding. Artificial Intelligence, 219, 40-66, [https://doi.org/10.1016/j.artint.2014.11.006](https://doi.org/10.1016/j.artint.2014.11.006).



[^1] We are aware of the variation in which the constraints created from a conflict are 3 stating, that agent $a_i$
should be on the node at time $t$, another stating that the second agent $a_j$ should be on the node at time $t$ and
one stating that neither of the agents can be on the node at time $t$. At the moment, we decided though that the best
course of action is to actually stick to the version described in [1].
